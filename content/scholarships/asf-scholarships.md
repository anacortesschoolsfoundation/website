---
title: "Scholarship Programs"
aliases:
  - /scholarships/scholarship-applications.html
  - /about-us/31-general/general/69-scholarship-applications.html
bannerImage: /uploads/images/page-title-scholarships.jpg
description: Applying for scholarships is easy! Submitting the ASF General Scholarship Application makes you eligible for most ASF scholarships, so there's no need to submit multiple applications for the many scholarships that ASF offers.
keywords: Anacortes Schools, Anacortes School District, scholarships, how to apply for scholarships, how do I apply for scholarships
featuredImage: /uploads/schol-1.jpg
---

<div class="row pb-4">
    <div class="col-lg-6 pb-4 d-md-none d-lg-block">
	<img src="//images.weserv.nl/?url=www.anacortesschoolsfoundation.org/uploads/deskin-2019-1.jpg&w=365" width="365" alt="Anacortes Schools Foundation Scholarships" class="rounded">
	</div>
    <div class="col-lg-5">
        <p>Over the past 37 years, the Anacortes Schools Foundation has awarded over 1000 scholarships and over $2
            million to Anacortes graduates.</p>
			<a href="/scholarships#all" class="button button-mini button-dark button-rounded">Browse All Scholarships</a>
	</div>
</div>
<div class="row align-items-top">
	<div class="col-lg-11">
	<p>Thanks to the incredible generosity of families, businesses and organizations, each spring, ASF continues
            this tradition of support, awarding over $300,000 in scholarships each year. One common application makes
            students eligible for over 120 scholarships.</p>
	<p>The ASF Scholarship Committee reviews the applications and the available scholarships and decides on the awards. IRS rules do not allow donors to select their scholarship recipients.</p>
	Some scholarships may be selected based on a combination of criteria, such as:
        <ul class="pl-4">
            <li>Academic performance</li>
            <li>Areas of interest</li>
            <li>Financial need</li>
            <li>Pursuing education in a specific field (such as STEM, the arts, international studies, etc.)</li>
            <li>Student's demonstrated willingness and ability to help others</li>
        </ul>
		<a href="/student-info" class="button button-mini button-dark button-rounded">Student Information</a>
		 </div>
    </div>
