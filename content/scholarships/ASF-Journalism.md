---
title: ASF Journalism Scholarship
est: 2023
areas: 
  - General
description: The ASF Journalism Scholarship is aimed at following the philosophy that a better informed public through honest reporting makes a better community.
keywords: scholarship, ASF Journalism, Art Shotwell, Lexie Lamborn
featuredImage: 
donorImage:
scholarship: true
recipients:
  - name: Dashaun Coutee
    year: 2023
    scholarshipImage: /uploads/ASF Journalism - Coutee, Dashaun.jpg
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This scholarship is established by Art Shotwell and Lexie Lamborn. Art worked in radio and tv news for more than 30 years in Los Angeles, San Diego, Honolulu and Seoul, Korea, earning three national awards and more than a handful of local awards, in addition to four honors for a small, local newspaper website. This scholarship is aimed at following Art's personal philosophy that, "A better informed public through honest reporting makes a better community."
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

