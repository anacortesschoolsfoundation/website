---
title: Bill and Dorothy "Ho" Taylor Scholarship
est: 2011
areas: 
  - Community Service
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, Bill Taylor, Dorothy Taylor
description: This scholarship is awarded to a student planning to attend a 4-year college who has demonstrated leadership skills, volunteerism.
donorImage:
recipients:
  - name: Ella Frost
    year: 2021
    scholarshipImage: /uploads/frost-ella.jpg 
  - name: Lauren Markovich
    year: 2021
    scholarshipImage: /uploads/markovich-lauren.jpg 
  - name: Maggie Betts
    year: 2020
    scholarshipImage: /uploads/betts-maggie.jpg 
  - name: Jade King
    year: 2019
    scholarshipImage: /uploads/king-jade.jpg 
  - name: P.S. Layne Woodward
    year: 2019
    scholarshipImage: 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Bill and Dorothy "Ho" Taylor made their home in Anacortes in the late 1940's and raised five children here. Bill, a teacher, coach and administrator at AHS, was instrumental in mentoring, serving as a positive role model and providing support to students. This continued over the entire course of his career. These students often needed an extra push to convince themselves that they could meet or exceed their potential. Ho, who met Bill while attending the University of Washington, was the daughter of a doctor and nurse in Butte, Montana. She was instrumental in raising her five children with Bill. Ho worked at a local jewelry store and was also instrumental in raising money to improve the value of the local hospital in Anacortes. Their outreach to others and the community they loved is why the family has dedicated this scholarship in their honor. 
              The scholarship is awarded to a student planning to attend a 4-year college who has progressively, through high school, demonstrated leadership skills and/or volunteerism, developed and maintained positive relationships with classmates and educators, and who shows commitment and follow-through with activities and goals. Consideration may also be given to a student who may have turned around their lives while in high school, overcome struggles with academics or become actively engaged in sports/school activities.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

