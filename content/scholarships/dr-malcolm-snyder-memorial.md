---
title: Dr. Malcolm Snyder Memorial Scholarship
est: 2002
areas: 
  - Community Service
  - Memorial
description: The Dr. Malcolm Snyder Memorial Scholarship is awarded to a student who shows good citizenship in the Anacortes community.
keywords: scholarship, Dr. Malcolm Snyder scholarship, Dr. Snyder scholarship
featuredImage: 
donorImage:
recipients:
  - name: Persephone Pestar
    year: 2023
    scholarshipImage: /uploads/Snyder - Pestar, Persephone.jpeg
  - name: Sofia (Grete) Dunton
    year: 2023
    scholarshipImage: /uploads/Snyder - Dunton, Sofia (Grete).jpg
  - name: Noah Hunter
    year: 2022
    scholarshipImage: 
  - name: Alison Perez
    year: 2022
    scholarshipImage: 
  - name: Chloe Chambers
    year: 2022
    scholarshipImage: /uploads/Malcom Snyder - Chambers, Chloe.jpg
  - name: Jade Carter
    year: 2019
    scholarshipImage: /uploads/carter-jade.jpg
  - name: Abigiail Hogge
    year: 2021
    scholarshipImage: 
  - name: Zoie Schwartz
    year: 2021
    scholarshipImage: /uploads/schwartz-zoie.jpg
  - name: Anthony Anderson
    year: 2020
    scholarshipImage: /uploads/anderson-anthony.jpg
  - name: Kaitlin McLaughlin
    year: 2020
    scholarshipImage: /uploads/mclaughlin-kaitlin.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The recipients have been awarded this scholarship due to their effort in volunteering.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

