---
title: Service Employees International Union Scholarship
est: 1998
areas: 
  - General
description: The Service Employees International Union Scholarship is funded through employee payroll deductions from Anacortes School District employees.
keywords: scholarship, Service Employees International Union, Service Employees International Union scholarship
featuredImage: 
donorImage:
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This award is granted by Anacortes School District employees including those employed in custodial, maintenance, bus drivers, and food service positions.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

