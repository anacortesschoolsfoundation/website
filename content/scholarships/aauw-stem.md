---
title: AAUW STEM
est: 2022
areas: 
  - STEM
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, 
description:  These STEM scholarships are established by The American Association of University Women (AAUW). 
donorImage:
recipients:
  - name: Abigail Ball
    year: 2023
    scholarshipImage: /uploads/AAUW - Ball, Abigail.jpeg
  - name: Ellie Feist
    year: 2023
    scholarshipImage: /uploads/AAUW - Feist, Ellie.jpeg
  - name: Severa Kulenkamp
    year: 2023
    scholarshipImage: /uploads/AAUW - Kulenkamp, Severa.jpg
  - name: Caitlin Brar
    year: 2022
    scholarshipImage: /uploads/AAUW STEM - Brar, Caitlin.jpg
  - name: Elle Carlson
    year: 2022
    scholarshipImage: /uploads/AAUW STEM - Carlson, Elle.jpg
  - name: Lucy Shainin
    year: 2022
    scholarshipImage: /uploads/AAUW STEM - Shainin, Lucy.JPG
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
               The AAUW empowers women and girls through research, education, and advocacy. The organization has more than 170,000 members and supporters across the United States, as well as 1,000 local branches and 800 college and university partners. Since the national organization's founding in 1881, members have examined and taken positions on the fundamental issues of the day — educational, social, economic, and political.  The Anacortes branch was founded in 1956 and to-date has funded over $120,000 in scholarships to local women. Scholarships will be awarded to high school seniors who are planning to take courses at the undergraduate level in preparation for a career in a STEM field.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

 