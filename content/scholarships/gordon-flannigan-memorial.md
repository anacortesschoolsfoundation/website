---
title: Gordon Flannigan Memorial Scholarship
est: 1997
areas: 
  - Technical or Vocational
  - Memorial
description: The Gordon Flannigan Memorial Scholarship is awarded to a deserving student who is bound for two-year vocational training and who has not received any other scholarships.
keywords: scholarship, Gordon Flannigan scholarship
featuredImage: 
donorImage:
recipients:
  - name: Harley Hough
    year: 2021
    scholarshipImage: /uploads/hough-harley.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Gordon Flannigan was AHS Senior Class President of the Class of 1941, football and basketball co-captain in 1940, Key Club Vice-President in 1940 and President in 1941. The Gordon Flannigan Memorial Scholarship is awarded to a deserving student who is bound for two-year vocational training and who has not received any other scholarships.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

