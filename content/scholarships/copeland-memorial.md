---
title: Copeland Memorial Scholarship
est: 2021
areas: 
  - STEM
  - Education
  - Memorial
description: This scholarship is established through the Jean L. Copeland Living Trust and is awarded to a student who plans to pursue higher education in the fields of mathematics, science, engineering or teaching.
keywords: scholarship, Jean Copeland, Jean Copeland scholarship, math scholarship, science scholarship, engineering scholarship, education scholarship, teaching scholarship
featuredImage: /uploads/Copeland-scholarship.jpg
donorImage: /uploads/Copeland-scholarship.jpg
recipients:
  - name: Grady Anthonysz-Knutson
    year: 2022
    scholarshipImage: /uploads/Copeland Memorial - Anthonysz-Knutson, Grady.jpg
  - name: Benjamin Fountain
    year: 2021
    scholarshipImage: /uploads/fountain-benjamin.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This scholarship is established through the Jean L. Copeland Living Trust and is awarded to a student who plans to pursue higher education in the fields of mathematics, science, engineering or teaching.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

