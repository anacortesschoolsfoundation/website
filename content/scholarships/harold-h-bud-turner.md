---
title: Harold H. "Bud" Turner Scholarship
est: 1990
areas: 
  - Memorial
  - STEM
description: The Harold H. "Bud" Turner Scholarship is given in recognition of Bud's unique abilities to combine American ingenuity with practical application towards facilitating and modernizing bulk product movement.
keywords: scholarship, technical scholarship, engineering scholarship, math scholarship, industrial sciences scholarship
featuredImage: 
donorImage:
recipients:
  - name: Ethan Miller
    year: 2023
    scholarshipImage: /uploads/Turner - Miller, Ethan.jpg
  - name: Jake Schuh
    year: 2022
    scholarshipImage: /uploads/Harold H. _Bud_ Turner - Schuh, Jake.jpg
  - name: Joshua Jakob Ocampo
    year: 2021
    scholarshipImage: /uploads/ocampo-joshua.jpg
  - name: Garrett Kennedy
    year: 2020
    scholarshipImage: /uploads/kannedy-garrett.jpg
  - name: Rafka Daou
    year: 2019
    scholarshipImage: /uploads/daou-rafka.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Bud always made each task a pleasure as well as a challenge with his sense of humor, friendship, and good nature. This scholarship is facilitated through the Port of Anacortes and is awarded to a graduating senior from Anacortes High School who exhibits strong technical skills and who plans on majoring in a technical field such as engineering, mathematics or industrial sciences.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

