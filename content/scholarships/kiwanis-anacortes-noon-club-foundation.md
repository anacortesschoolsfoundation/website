---
title: Kiwanis Anacortes Noon Club Foundation Scholarship
est: 2019
areas: 
  - General
description: Kiwanis is a world-wide organization whose focus is "Serving the Children of the World." Kiwanis raises funds for scholarships and other community projects through a variety of fundraising efforts including the Kiwanis Thrift Store.
keywords: scholarships, Kiwanis Noon scholarship, Kiwanis Anacortes Noon Club
featuredImage: /uploads/donor-kiwanis-noon.jpg
donorImage: /uploads/donor-kiwanis-noon.jpg
recipients:
  - name: Sam Davis
    year: 2023
    scholarshipImage: /uploads/KN - Davis, Sam.JPG
  - name: Parker King
    year: 2023
    scholarshipImage: /uploads/KN - King, Parker.jpg
  - name: Macy Koegel
    year: 2023
    scholarshipImage: /uploads/KN - Koegel, Macy.jpeg
  - name: Makishi Mott
    year: 2023
    scholarshipImage: /uploads/KN - Mott, Makishi.jpg
  - name: Fletcher Olson
    year: 2023
    scholarshipImage: /uploads/KN - Olson, Fletcher.jpg
  - name: Madelyn Shelling
    year: 2023
    scholarshipImage: /uploads/KN - Shelling, Madelyn.jpg
  - name: Tyler Blouin
    year: 2023
    scholarshipImage: /uploads/KN (Beeler) - Blouin, Tyler.JPG
  - name: McKernan Boland
    year: 2023
    scholarshipImage: /uploads/KN (Key Club) - Boland, McKernan.jpg
  - name: Abbie Goodwin
    year: 2023
    scholarshipImage: /uploads/KN (Key Club) - Goodwin, Abbie.jpg
  - name: Patrick Quinn
    year: 2023
    scholarshipImage: /uploads/KN (Norton) - Quinn, Patrick.jpg
  - name: Olivia Schwartz
    year: 2022
    scholarshipImage: 
  - name: Reese Vaux
    year: 2022
    scholarshipImage: 
  - name: Alicia Ambriz-Espino
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon (Johnson) - Ambriz-Espino, Alicia.jpg
  - name: Megan Carroll
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon (Norton) - Carroll, Megan.jpg
  - name: William Waldrop
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon (Wright) - Waldrop, William.jpg
  - name: Zachary Bowman
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - Bowman, Zachary.jpg
  - name: Elizabeth Braatz
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - Braatz, Elizabeth.jpg
  - name: Alyssa Digweed
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - Digweed, Alyssa.jpg
  - name: Aynslee King
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - King, Aynslee.JPG
  - name: Sarah Quinn
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - Quinn, Sarah.jpg
  - name: Kaitlyn Sage
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - Sage, Kaitlyn.JPG
  - name: Lindsey South
    year: 2022
    scholarshipImage: /uploads/Kiwanis Noon - South, Lindsey.JPG 
  - name: Hannah Cross
    year: 2021
    scholarshipImage: /uploads/cross-hannah.jpg
  - name: Madison DeBruler
    year: 2021
    scholarshipImage:
  - name: Joshua Digweed
    year: 2021
    scholarshipImage: 
  - name: Vince Jackson
    year: 2021
    scholarshipImage: /uploads/jackson-vincent.jpg
  - name: Matthew Lowrie
    year: 2021
    scholarshipImage: /uploads/lowrie-matthew.jpg
  - name: Ava Martin
    year: 2021
    scholarshipImage: 
  - name: Jaida Mason
    year: 2021
    scholarshipImage: /uploads/mason-jaida.jpg
  - name: Liam Patrick
    year: 2021
    scholarshipImage: /uploads/patrick-liam.jpg
  - name: Kaitlyn Sage
    year: 2021
    scholarshipImage: /uploads/sage-kaitlyn.jpg
  - name: Eliza Senff
    year: 2021
    scholarshipImage: /uploads/senff-eliza.jpg
  - name: Gabriella Zumpano
    year: 2021
    scholarshipImage: /uploads/zumpano-gabriella.jpg
  - name: Jefferson Butler
    year: 2020
    scholarshipImage: /uploads/butler-jefferson.jpg
  - name: Brandon Cross
    year: 2020
    scholarshipImage: /uploads/cross-brandon.jpg
  - name: Hannah Cross
    year: 2020
    scholarshipImage: /uploads/cross-hannah.jpg
  - name: Grayson Eaton
    year: 2020
    scholarshipImage: /uploads/eaton-grayson.jpg
  - name: Kaeden Flynn
    year: 2020
    scholarshipImage: /uploads/flynn-kaedan.jpg
  - name: Samuel Hardesty
    year: 2020
    scholarshipImage: /uploads/hardesty-samuel.jpg
  - name: Vincent Jackson
    year: 2020
    scholarshipImage: /uploads/jackson-vincent.jpg
  - name: Clare Martin
    year: 2020
    scholarshipImage: /uploads/martin-clare.jpg
  - name: Liam Patrick
    year: 2020
    scholarshipImage: /uploads/patrick-liam.jpg
  - name: Sarah Quinn
    year: 2020
    scholarshipImage: /uploads/quinn-sarah.jpg
  - name: Anja Shjarback
    year: 2020
    scholarshipImage: /uploads/shjarback-anja.jpg
  - name: Baylee South
    year: 2020
    scholarshipImage: /uploads/south-baylee.jpg
  - name: Cabry Biddle
    year: 2019
    scholarshipImage: /uploads/biddle-cabry.jpg
  - name: Zack Bowman
    year: 2019
    scholarshipImage: /uploads/bowman-zack.jpg
  - name: Nicole Bunzel
    year: 2019
    scholarshipImage: 
  - name: Brandon Cross
    year: 2019
    scholarshipImage: /uploads/cross-brandon.jpg
  - name: Hannah Cross
    year: 2019
    scholarshipImage: /uploads/cross-hannah.jpg
  - name: Kara Foster
    year: 2019
    scholarshipImage: /uploads/foster-kara.jpg
  - name: Christoph Franssen
    year: 2019
    scholarshipImage: /uploads/franssen-christoph.jpg
  - name: Skye Hopper
    year: 2019
    scholarshipImage: /uploads/hopper-skye.jpg
  - name: Jonathan Izquierdo-Salas
    year: 2019
    scholarshipImage: /uploads/izquierdo-salas-jonathan.jpg
  - name: Sadie Leavitt
    year: 2019
    scholarshipImage: /uploads/leavitt-sadie.jpg
  - name: Ava Martin
    year: 2019
    scholarshipImage: /uploads/martin-ava.jpg
  - name: Will McKracken
    year: 2019
    scholarshipImage: /uploads/mckracken-will.jpg
  - name: Liam Patrick
    year: 2019
    scholarshipImage: /uploads/patrick-liam.jpg
  - name: Conor Powell
    year: 2019
    scholarshipImage: /uploads/powell-conor.jpg
  - name: Evan Rodriguez
    year: 2019
    scholarshipImage: /uploads/rodriguez-evan.jpg
  - name: Riley Ward
    year: 2019
    scholarshipImage: /uploads/ward-riley.jpg
  - name: Christopher Warmuth
    year: 2019
    scholarshipImage: 
  - name: Clare Martin
    year: 2021
    scholarshipImage: /uploads/martin-clare-2.jpg
  - name: Lindsey South
    year: 2020
    scholarshipImage: /uploads/south-lindsey.jpg
  - name: Kaitlin Banta
    year: 2019
    scholarshipImage: /uploads/banta-kaitlin.jpg
  - name: Alexander Carroll
    year: 2021
    scholarshipImage: /uploads/carroll-alexander.jpg
  - name: Katelyn Hellman
    year: 2020
    scholarshipImage: /uploads/hellman-katelyn.jpg
  - name: Alex Knudtson
    year: 2019
    scholarshipImage: 
  - name: Cooper Nichols
    year: 2021
    scholarshipImage: /uploads/nichols-cooper.jpg
  - name: Jake Romag
    year: 2020
    scholarshipImage: /uploads/romag-jake.jpg
  - name: Gabriellia Erb
    year: 2021
    scholarshipImage: /uploads/erb-gabriellia.jpg
  - name: Jonathan Izquierdo-Salas
    year: 2019
    scholarshipImage: /uploads/izquierdo-salas-jonathan.jpg    
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Kiwanis Anacortes Noon Club Foundation facilitates a number of scholarships each year. Kiwanis is a world-wide organization whose focus is "Serving the Children of the World." Kiwanis raises funds for scholarships and other community projects through a variety of fundraising efforts including the Kiwanis Thrift Store. The amount and number of scholarships varies annually. A designated amount may support students going on to 4-year universities, a designated amount may support current college students and a designated amount may also support technical, vocational or trade school bound students.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

