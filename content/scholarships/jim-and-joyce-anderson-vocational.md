---
title: Jim and Joyce Anderson Vocational Scholarship
est: 2019
areas: 
  - Technical or Vocational
description: The Andersons believe not everyone fits into a 4-year program and established this scholarship to support vocational workers such as plumbers, plasterers, welders, boat mechanics and those pursuing the culinary arts.   
keywords: scholarship, Jim Anderson scholarship, Joyce Anderson scholarship, vocational scholarship, plumber scholarship, plasterer scholarship, welding scholarship, boat mechanic scholarship, culinary arts scholarship
featuredImage: 
donorImage:
recipients:
  - name: Moses Pittis
    year: 2023
    scholarshipImage: /uploads/Anderson - Pittis, Moses.JPG
  - name: Alicia Ambriz-Espino
    year: 2022
    scholarshipImage: /uploads/Jim _ Joyce Anderson - Ambriz-Espino, Alicia.jpg
  - name: Haley Treichel
    year: 2022
    scholarshipImage: /uploads/Jim _ Joyce Anderson - Treichel, Haley.jpg
  - name: Emma Bakke
    year: 2021
    scholarshipImage: /uploads/bakke-emma.jpg
  - name: Jayden Frydenlund
    year: 2021
    scholarshipImage: /uploads/frydenlund-jayden.jpg
  - name: Katelyn Hellman
    year: 2020
    scholarshipImage: /uploads/hellman-katelyn.jpg
  - name: Antonio Hernandez Rodriguez
    year: 2019
    scholarshipImage: /uploads/hernandez-rodriguez-antonio.jpg
  - name: Lauren Post
    year: 2019
    scholarshipImage: /uploads/post-lauren.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              We will need more vocational workers such as plumbers, plasterers, welders, boat mechanics and those pursuing the culinary arts. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

  