---
title: Anacortes Schools Foundation Scholarship
est: 2006
areas: 
  - General
featuredImage: /uploads/donor-asf.jpg
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, ASF
description:  Awards are funded through generous donations from community members (some anonymously) and from ASF Board Members.
donorImage: /uploads/donor-asf.jpg
recipients:
  - name: Ashley Millegan
    year: 2023
    scholarshipImage: /uploads/ASF Science - Millegan, Ashley.JPG
  - name: Breann Morgenthaler
    year: 2023
    scholarshipImage: /uploads/ASF - Morgenthaler, Breann.JPG
  - name: Jacob Hayes
    year: 2023
    scholarshipImage: /uploads/ASF - Hayes, Jacob.jpg
  - name: Brittany Goss
    year: 2023
    scholarshipImage: /uploads/ASF - Goss, Brittany.JPG    
  - name: Jack Carlson
    year: 2023
    scholarshipImage: /uploads/ASF - Carlson, Jack.jpg
  - name: Alexandra Bielitz
    year: 2023
    scholarshipImage: /uploads/ASF - Bielitz, Alexandra.jpeg    
  - name: Sophia Carlson
    year: 2022
    scholarshipImage: /uploads/ASF - Carlson, Sophia.jpg
  - name: Ellie Notaro
    year: 2022
    scholarshipImage: /uploads/ASF - Notaro, Ellie.jpg
  - name: Hannah Peak
    year: 2022
    scholarshipImage: /uploads/ASF - Peak, Hannah.jpg
  - name: Victoria Pittman
    year: 2022
    scholarshipImage: /uploads/ASF - Pittman, Victoria.jpg
  - name: Kirsha Khile
    year: 2022
    scholarshipImage: /uploads/ASF Donor - Khile, Kirsha.jpg
  - name: Gabriel Burnett
    year: 2022
    scholarshipImage: 
  - name: Kassandra Jenson
    year: 2022
    scholarshipImage: 
  - name: Matthew Lujan
    year: 2022
    scholarshipImage: 
  - name: Grace Tesch
    year: 2022
    scholarshipImage: 
  - name: Ella Villines
    year: 2022
    scholarshipImage: 
  - name: Gabriel Burnett
    year: 2022
    scholarshipImage: 
  - name: Jessica Augustoni
    year: 2021
    scholarshipImage: /uploads/augustoni-jessica.jpg
  - name: Brendan Hodgson
    year: 2021
    scholarshipImage:
  - name: Charlie Reyerson
    year: 2021
    scholarshipImage:
  - name: Abigail Schnabel
    year: 2021
    scholarshipImage: /uploads/schnabel-abigail.jpg
  - name: Gabriella Snowman
    year: 2021
    scholarshipImage:
  - name: Aaliyah Spencer
    year: 2021
    scholarshipImage:
  - name: Grace Anne Evans
    year: 2021
    scholarshipImage: /uploads/evans-grace-anne.jpg
  - name: Hope Evans
    year: 2021
    scholarshipImage: /uploads/evans-hope.jpg
  - name: Olivia Feist
    year: 2021
    scholarshipImage: /uploads/feist-olivia.jpg
  - name: Joey Keltner
    year: 2021
    scholarshipImage: /uploads/keltner-joey.jpg
  - name: Brigid Mack
    year: 2021
    scholarshipImage: /uploads/mack-brigid.jpg
  - name: Lauren McClintock
    year: 2021
    scholarshipImage: /uploads/mcclintock-lauren.jpg
  - name: Teja Rasmussen
    year: 2021
    scholarshipImage: /uploads/rasmussen-teja.jpg
  - name: Maia Stovel
    year: 2021
    scholarshipImage:
  - name: Grace Tesch
    year: 2021
    scholarshipImage: /uploads/tesch-grace.jpg
  - name: Heidi Vandyk
    year: 2021
    scholarshipImage:
  - name: Zoe Yanega
    year: 2021
    scholarshipImage: /uploads/yanega-zoe.jpg
  - name: Elizabeth (Ellie) Chambers
    year: 2020
    scholarshipImage: /uploads/chambers-ellie.jpg
  - name: Abigail Hylton
    year: 2020
    scholarshipImage: /uploads/hylton-abby.jpg
  - name: Hannah Loesch
    year: 2020
    scholarshipImage: /uploads/loesch-hannah.jpg
  - name: Macy Yanega
    year: 2020
    scholarshipImage: /uploads/yanega-macy.jpg
  - name: Alexa Campos 
    year: 2019 
    scholarshipImage: /uploads/campos-alexa.jpg 
  - name: Reese Vaux 
    year: 2019 
    scholarshipImage: /uploads/vaux-reese.jpg 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Anacortes Schools Foundation Scholarships celebrate ASF's commitment to scholarships since 1982. Awards are funded through generous donations from community members (some anonymously) and from ASF Board Members. Awards are based on academic achievement (3.0 GPA or higher) and financial need may be a consideration. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

