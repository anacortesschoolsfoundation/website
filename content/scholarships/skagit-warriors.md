---
title: Skagit Valley Warriors Female Athlete Scholarship
est: 2023
areas: 
  - Sports
featuredImage: 
keywords: Skagit Valley Warriors scholarhip, womens football scholarship Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, 
description: The Skagit Valley Warriors charity women's football team raises money for local youth in our community. The Skagit Valley Warriors want to encourage women, empower women, and support female athletes in our community. 
featuredImage: /uploads/donor-svwarriors.jpg
donorImage: /uploads/donor-svwarriors.jpg
recipients:
  - name: Erin Kennedy
    year: 2023
    scholarshipImage: /uploads/Warriors - Kennedy, Erin.jpeg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              They are offering a one year scholarship to an Anacortes High School graduating senior Female Athlete (including cheer and dance) who participated in a sport for at least 2 years during high school. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
