---
title: Ann W. Nord Memorial Nursing Scholarship
est: 2015
areas: 
  - Memorial
  - Health and Human Services
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, Ann Nord Scholarship, nursing scholarship
description: The scholarship seeks to encourage outstanding graduates with interest and commitment in the field of nursing.
donorImage:
recipients:
  - name: Whitney Hogge
    year: 2022
    scholarshipImage: /uploads/Ann W. Nord Memorial - Hogge, Whitney.jpg
  - name: Nicole McInerney
    year: 2021
    scholarshipImage:
  - name: Hannah Kaleel
    year: 2019
    scholarshipImage: 
  - name: Miranda Young
    year: 2019
    scholarshipImage: /uploads/young-miranda.jpg  
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Ann W. Nord graduated from the University of Utah College of Nursing, and spent a major part of the next forty years working as a hospital-based clinical nurse in Salt Lake City, Utah. Upon retirement she moved to Anacortes with her husband, Dr. Nathaniel M. Nord, where they resided until her passing in 2013. The family, as well as Ann's many friends and admirers, established the Ann W. Nord Memorial Nursing Scholarship to honor Ann and her lifelong appreciation of education, community participation, and seriousness of purpose in her chosen profession.
              The scholarship seeks to encourage outstanding Anacortes High School graduates with interest and commitment in the field of nursing to pursue an educational program which stresses the importance of the study of the liberal arts and humanities as a preparation for achieving a Bachelor of Science Degree in Nursing (BSN) and a career in nursing.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---