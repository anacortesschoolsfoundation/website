---
title: Bame Basketball Award
est: 2018
areas: 
  - Sports
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, Bame basketball award, bame basketball scholarship, bame, basketball, scholarship
description: The Bame Basketball Award was established John Bame to encourage Anacortes High School Varsity Basketball players to do well academically and go on to college.
donorImage:
recipients:
  - name: Tyler Blouin
    year: 2019
    scholarshipImage: 
  - name: Gabby Ronngren
    year: 2019
    scholarshipImage: /uploads/ronngren-gabby.jpg 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The award is intended for a student who plans to play basketball at the college/ community college level.  
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

