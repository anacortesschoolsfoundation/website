---
title: Leroy Hirni Career and Technical Education Scholarship
est: 2019
areas: 
  - Technical or Vocational
  - Memorial
description: This scholarship honors the memory of Leroy "Mr. Kiwanis" Hirni and his outstanding contributions to the Anacortes community. 
keywords: scholarship, Leroy Hirni, technical school scholarship
featuredImage: 
donorImage:
recipients:
  - name: Bernardo Manangan
    year: 2023
    scholarshipImage: /uploads/Hirni - Manangan, Bernardo.jpg
  - name: Brayden Davis
    year: 2022
    scholarshipImage: /uploads/Leroy Hirni - Davis, Brayden.jpg
  - name: Adayla Rogers
    year: 2021
    scholarshipImage: 
  - name: Alaina Schafer
    year: 2021
    scholarshipImage: /uploads/schafer-alaina.jpg
  - name: Emma Bakke
    year: 2020
    scholarshipImage: /uploads/bakke-emma.jpg
  - name: Jadan White
    year: 2020
    scholarshipImage: /uploads/white-jadan.jpg
  - name: Emma Bakke
    year: 2019
    scholarshipImage: /uploads/bakke-emma-2019.jpg
  - name: Nicholas Navarro
    year: 2019
    scholarshipImage: /uploads/navarro-nicholas.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Leroy Hirni was an active and beloved member of the Noon Kiwanis Club of Anacortes. He was a mechanic in WWII and had great appreciation for those who worked with their hands and in trade professions. As a member of Kiwanis, Leroy was key in establishing the Kiwanis Thrift Store which continues to thrive today and support many community efforts. He was also an active member of the Anacortes School Board. Leroy is described as a man of principle and as someone who had a lot of moxie and got things done. This scholarship is awarded to a student or students who plan to pursue a trade, technical or vocational career path. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

