---
title: Kirvin Smith Memorial Scholarship
est: 2001
areas: 
  - Memorial
description: The Kirvin Smith Memorial Scholarship is offered in his memory and is awarded to an Anacortes High School senior, who plans to attend a 4-year college.
keywords: scholarship, Kirvin Smith, Kirvin Smith scholarship
featuredImage: 
donorImage:
recipients:
  - name: Kyan Bauer
    year: 2022
    scholarshipImage: 
  - name: Balihar Sandhu
    year: 2021
    scholarshipImage: 
  - name: Sophie Riley
    year: 2020
    scholarshipImage: /uploads/riley-sophie2.jpg
  - name: Nhi Ngo
    year: 2019
    scholarshipImage: /uploads/ngo-nhi.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Kirvin Smith was a long-time, well-liked and highly respected history teacher and Senior Class Advisor at Anacortes High School from 1936 to 1966.  Mr. Smith is remembered for his dedication to his profession and for being generous with his time--he was always available to help out on projects and special assignments. He was civic minded, as evidenced by his years of active participation in the Anacortes Kiwanis Club, and he shared his passion for community service with his students. His wife, Anne, taught first grade in the district also for many years. The Kirvin Smith Memorial Scholarship is offered in his memory and is awarded to an Anacortes High School senior, who plans to attend a 4-year college. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

