---
title: George L. Keys Memorial Scholarship
est: 1986
areas: 
  - Memorial
description: The George L. Keys Memorial Scholarship honors the memory of George Keys. 
keywords: scholarship, George Keys scholarship
featuredImage:  
donorImage:
recipients:
  - name: Alison Perez
    year: 2022
    scholarshipImage: 
  - name: Alaura Swanson
    year: 2021
    scholarshipImage: /uploads/swanson-alaura.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This award is presented to a deserving female student who exhibits a strong desire to succeed and shows evidence of good character.  
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
