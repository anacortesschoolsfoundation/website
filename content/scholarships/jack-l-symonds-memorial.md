---
title: Jack L. Symonds Memorial Scholarship
est: 2001
areas: 
  - Memorial
description: The Jack L. Symonds Memorial Scholarship is awarded to a student who shows good character and responsible citizenship.
keywords: scholarship, sports scholarship, anacortes sports scholarship
featuredImage: 
donorImage:
recipients:
  - name: John Hernandez
    year: 2023
    scholarshipImage: /uploads/Symonds - Hernandez, John.jpg
  - name: Aidan Pinson
    year: 2022
    scholarshipImage: 
  - name: Adayla Rogers
    year: 2021
    scholarshipImage: 
  - name: Lindsey South
    year: 2020
    scholarshipImage: /uploads/south-lindsey.jpg
  - name: Keegan Kennedy
    year: 2019
    scholarshipImage: /uploads/kennedy-keegan.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Jack L. Symonds Memorial Scholarship is awarded to a student who shows good character and responsible citizenship. Jack was an outstanding athlete at AHS, so the recipient must have also actively participated in some kind of sports for two or three years in high school. The award is made annually to a graduating senior.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

