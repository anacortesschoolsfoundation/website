---
title: Okerlund Family Trust Scholarship
est: 2003
areas: 
  - Memorial
description: Emmett Okerlund's wish was to pay back the community for the education he so valued by providing scholarships to help promising students expand their horizons through a college education. 
keywords: scholarship, Emmett Okerlund, Emmett Okerlund scholarship
featuredImage: /uploads/donor-okerlund.jpg
donorImage: /uploads/donor-okerlund.jpg
recipients:
  - name: Will McClintock
    year: 2023
    scholarshipImage: /uploads/Okerlund - McClintock, Will.jpg
  - name: Connor Barton
    year: 2022
    scholarshipImage: /uploads/Okerlund - Barton, Connor.jpg
  - name: Isabel Shainin
    year: 2022
    scholarshipImage: /uploads/Okerlund - Shainin, Isabel.JPG
  - name: Hunter Berard
    year: 2021
    scholarshipImage: /uploads/berard-hunter.jpg
  - name: Graham Quitno
    year: 2021
    scholarshipImage: /uploads/quitno-graham.jpg
  - name: Ava Hawkins
    year: 2020
    scholarshipImage: /uploads/hawkins-ava.jpg
  - name: Anna Prewitt
    year: 2020
    scholarshipImage: /uploads/prewitt-anna.jpg
  - name: Oleksii Bondar
    year: 2019
    scholarshipImage: /uploads/bondar-oleksii.jpg
  - name: Thomas Dylan
    year: 2019
    scholarshipImage: 
  - name: Alexandra Hanesworth
    year: 2019
    scholarshipImage: 
  - name: Julia Houppermans
    year: 2019
    scholarshipImage: 
  - name: Nicole Houppermans
    year: 2019
    scholarshipImage: /uploads/houppermans-nicole.jpg
  - name: Sierra Scamfer
    year: 2019
    scholarshipImage: 
  - name: Sally Vaux
    year: 2019
    scholarshipImage: 
  - name: Lydia Weddle
    year: 2019
    scholarshipImage: 
  - name: Calder Wood
    year: 2019
    scholarshipImage: 
  - name: Giulia Wood
    year: 2019
    scholarshipImage: 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Emmett Okerlund, the youngest of seven children, grew up in Anacortes in a family of modest means. After graduation from AHS, he enlisted in the Navy, fought in World War II and worked as a salesman for a paper company in Bellingham. Upon retirement, Mr. Okerlund and his wife, Helen, were able to move back to Washington State and settled, for the last time, at Hood Canal, where he was able to do what he loved best—boating and fishing. He died in Shelton, Washington in November 2003, at the age of 90. Mr. Okerlund treasured his education in Anacortes and left his entire estate to the Anacortes Schools Foundation. His wish was to pay back the community for the education he so valued by providing scholarships to help promising students expand their horizons through a college education. He also hoped that recipients of his generosity would one day follow his example and fund an Anacortes Schools Foundation Scholarship for another deserving student. These scholarships are awarded to students with exemplary academic achievement. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

