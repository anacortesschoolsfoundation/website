---
title: Dorothy Ann Lione Nursing Scholarship
est: 2022
areas: 
    - Health and Human Services
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, 
description:  This scholarship is established by Mark Lione and is awarded to a senior who intends to pursue a career in nursing. 
featuredImage: /uploads/DorothyAnnLioneScholarship.jpg
donorImage: /uploads/DorothyAnnLioneScholarship.jpg
recipients:
  - name: Mileen Gallardo
    year: 2023
    scholarshipImage: /uploads/Lione - Gallardo, Mileen.jpg
  - name: Alyssa Digweed
    year: 2022
    scholarshipImage: /uploads/Lione Nursing - Digweed, Alyssa.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
               It is given in honor of Dorothy Ann Lione, an amazing woman, mother, leader, and nurse who had a robust and full life of 100 years. Dorothy was born in Queens, New York in 1921. She was raised by a single mom who taught Dorothy to be independent and decisive. She was the first woman president of the student body at Flushing High School, on the debate team, and a cheerleader all while working at the Flushing YMCA. She was the first person in her family to attend college and graduated with her nursing degree. She loved to dance, hike, travel, explore, see live theatre and opera watch hockey, baseball, football, and most of all basketball. She was the most positive person who always recognized the best in everyone.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

