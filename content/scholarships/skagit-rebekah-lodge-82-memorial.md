---
title: Skagit Rebekah Lodge 82 Memorial Scholarship
est: 1996
areas: 
  - Memorial
description: The Skagit Rebekah Lodge 82 Memorial Scholarship is awarded to a young man or woman graduating from Anacortes High School.
keywords: Scholarship, Skagit Rebekah Lodge 82
featuredImage: 
donorImage:
recipients:
  - name: Kaden Jacobson
    year: 2022
    scholarshipImage: /uploads/Skagit Rebekah - Jacobson, Kaden.jpg
  - name: Esther (Sienna) Gallahar
    year: 2021
    scholarshipImage: 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This award goes to student who are deserving, but who might not otherwise receive a scholarship. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
