---
title: E.S. Red and Marilyn Bell Scholarship
aliases:
est: 2022
areas: 
  - TECHNICAL OR VOCATIONAL
description: As longtime members of the Fidalgo Masonic Lodge and Eastern Star, Red and Marilyn Bell have supported scholarships for many years. 
keywords: scholarship, Technical scholarship, vocational scholarship, Bell Scholarship
featuredImage: /uploads/donor-bell.jpg
donorImage: /uploads/donor-bell.jpg
recipients:
  - name: Moses Pittis
    year: 2023
    scholarshipImage: /uploads/Bell - Pittis, Moses.JPG
  - name: McKenzie Blouin
    year: 2022
    scholarshipImage: /uploads/Bell - Blouin, McKenzie.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              In 2022, they established this scholarship to assist a motivated student who has set goals and either needs or merits help to achieve them. Red and Marilyn have a particular interest in supporting students who are pursuing higher education in a trade or at a technical school. Red and Marilyn have lived in Anacortes for 61 years. All 4 of their kids - David, Daniel, Dustin and Nancy all graduated from Anacortes High School. Red spent 33 years working at the Puget Sound Refinery.

      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---