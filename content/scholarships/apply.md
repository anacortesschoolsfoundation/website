---
title: "How to Apply for Scholarships"
aliases:
  - /scholarships/scholarship-applications.html
  - /about-us/31-general/general/69-scholarship-applications.html
bannerImage: /uploads/images/page-title-scholarships.jpg
description: Applying for scholarships is easy! Submitting the ASF General Scholarship Application makes you eligible for most ASF scholarships, so there's no need to submit multiple applications for the many scholarships that ASF offers.
keywords: Anacortes Schools, Anacortes School District, scholarships, how to apply for scholarships, how do I apply for scholarships
featuredImage: /uploads/schol-1.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: How to Apply for Scholarships
    bannerImage: /uploads/images/page-title-scholarships.jpg
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading: ASF General Scholarship Application Walkthrough
            youtubeID: uklX9Wn14tI
          - _bookshop_name: content
            title: Applying for scholarships is easy!
            content: |
              This year ASF has transitioned to an online application. Submitting the ASF General Scholarship Application makes you eligible for most ASF scholarships, so there's no need to submit multiple applications for the many scholarships that ASF offers!

              {{< apply-button >}}

              If you have any questions about the application process, please email [scholarships@asfkids.org](mailto:scholarships@asfkids.org).

              ### Graduating Seniors:

              For graduating seniors, be sure to have the following documents available for upload as they are required to complete your application:

              * Resume
              * Official Transcripts (must be stamped by the registrar)
              * Student Aid Report (SAR) (if you would like to be considered for financial need awards)

              ### Current College Students:

              For current college students, be sure to have the following documents available for upload as they are required to complete your application:

              * Personal Statement
              * Resume
              * Official Transcripts (must be stamped by the registrar)
              * Student Aid Report (SAR) (if you would like to be considered for financial need awards)
      - name: Dates
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
            sticky: true
---
