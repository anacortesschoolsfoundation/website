---
title: Chuck and Kay Starkovich Scholarship
est: 2021
areas: 
  - Technical or Vocational
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, Chuck Starkovich, Kay Starkovich, starkovich scholarship
description: This award goes to a student pursuing a 2-year degree, a CTE path, vocational, or technical program.
featuredImage: 
donorImage:
recipients:
  - name: Lacey Stickles
    year: 2023
    scholarshipImage: /uploads/Starkovich - Stickles, Lacey.jpg
  - name: Alyssa Jensen
    year: 2022
    scholarshipImage: /uploads/Chuck _ Kay Starkovich - Jensen, Alyssa.jpg
  - name: Isabella Sikic
    year: 2022
    scholarshipImage: /uploads/Chuck _ Kay Starkovich - Sikic, Isabella.jpg
  - name: Alyssa Jensen
    year: 2021
    scholarshipImage: /uploads/jensen-alyssa.jpg
  - name: Jazzmin Serrano
    year: 2021
    scholarshipImage:
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Chuck and Kay Starkovich Scholarship for vocational or CTE studies was established by Angela and Chad Currie, Tyler and Monique Starkovich and Tim and Tara Starkovich in honor of their parent's 50th Wedding Anniversary and in honor of their dedication and devotion to teaching. Chuck and Kay were long time elementary educators in the Anacortes School District, with Chuck teaching most of his forty-two years at Fidalgo Elementary School and Kay teaching all of her twenty-five years at Mt. Erie Elementary School. This award goes to a student pursuing a 2-year degree, a CTE path, vocational, or technical program.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

