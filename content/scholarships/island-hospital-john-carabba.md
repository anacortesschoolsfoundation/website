---
title: Island Hospital Scholarship in Memory of S.A. "John" Carabba
est: 2011
areas: 
  - Health and Human Services
  - Community Service
  - Memorial
description: This scholarship was established by Island Hospital in 2010 to recognize and honor Mr. Carabba's contributions and accomplishments. It is their greatest hope that the recipient of this award will follow his example of dedication to community health and community service.
keywords: scholarship, John Carabba, community service scholarship, community health scholarship
featuredImage: /uploads/donor-IH.png
donorImage: /uploads/donor-IH.png
recipients:
  - name: Abby Ries
    year: 2022
    scholarshipImage: 
  - name: Kunthida Puengpoh
    year: 2022
    scholarshipImage: /uploads/Island Health - Puengpoh, Kunthida.JPG
  - name: Madeline Lowrie
    year: 2021
    scholarshipImage: /uploads/lowrie-madeline.jpg
  - name: Olivia Schwartz
    year: 2021
    scholarshipImage: /uploads/schwartz-olivia.jpg
  - name: Ellison Kephart
    year: 2020
    scholarshipImage: /uploads/kephart-ellison.jpg
  - name: Isabelle Willoughby
    year: 2020
    scholarshipImage: /uploads/willoughby-isabelle.jpg
  - name: Sadie Leavitt
    year: 2019
    scholarshipImage: /uploads/leavitt-sadie.jpg
  - name: Brenna Palmer-Perry
    year: 2019
    scholarshipImage: /uploads/palmer-perry-brenna.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Two scholarships are awarded annually by Island Hospital in honor of dedicated health care employee S.A.'John' Carraba. John Carabba contributed to Island Hospital and the health of our community on a continual basis, starting in the early 1960's until his death in 2009. In 1977, he was instrumental in establishing the Island Hospital Health Foundation, as it was known then, and he served on the Board of the Foundation for over 30 years. He helped raise funds for an expanded Intensive Care Unit and modernization of the emergency room. His legacy is deep, with service to the U.S. Navy in the South Pacific, many local service clubs and boards and as a successful entrepreneur. He built the first local airport and owned several businesses that, over five decades, employed many Fidalgo Island youth. The Island Hospital Foundation established this scholarship in 2010 to recognize and honor Mr. Carabba's contributions and accomplishments, and it is their greatest hope that the recipient of this award will follow his example of dedication to community health and community service.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

