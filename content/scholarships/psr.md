---
title: HF Sinclair STEM Scholarships
aliases:
    - /scholarships/scholarships-66/93-shell-puget-sound-refinery-community-scholarship.html
    - /scholarships/shell-puget-sound-refinery-community/  
est: 2008
areas: 
  - STEM
description: Each year, ASF and HF Sinclair Puget Sound Refinery partner to present the Fueling Education Fun Run, which provides funding for several STEM scholarships.
keywords: scholarship, STEM scholarship, Shell scholarship, PSR, HF Sinclair
featuredImage: /uploads/donor-hfsinclair.jpg
donorImage: /uploads/donor-hfsinclair.jpg
recipients:
  - name: Bryan Brar
    year: 2023
    scholarshipImage: /uploads/HFS - Brar, Bryan.jpg
  - name: Noah Dunham
    year: 2023
    scholarshipImage: /uploads/HFS - Dunham, Noah.jpg
  - name: Landen Frost
    year: 2023
    scholarshipImage: /uploads/HFS - Frost, Landen.jpg
  - name: Mica Gold
    year: 2023
    scholarshipImage: /uploads/HFS - Gold, Mica.jpg
  - name: Jake Schuh
    year: 2023
    scholarshipImage: /uploads/HFS - Schuh, Jake.jpg
  - name: Logan Hilyer
    year: 2022
    scholarshipImage: 
  - name: Tuscan Harper
    year: 2022
    scholarshipImage: /uploads/PSR - Harper, Tuscan.jpg
  - name: Cooper Nichols
    year: 2022
    scholarshipImage: /uploads/PSR - Nichols, Cooper.jpg
  - name: Peter Walters
    year: 2022
    scholarshipImage: /uploads/PSR - Walters, Peter.jpg
  - name: Hannah Weeks
    year: 2022
    scholarshipImage: /uploads/PSR - Weeks, Hannah.jpg
  - name: Christian Pol Manangan
    year: 2021
    scholarshipImage: 
  - name: Alek Miller
    year: 2021
    scholarshipImage: /uploads/miller-alek.jpg
  - name: Sarah Quinn
    year: 2021
    scholarshipImage: /uploads/quinn-sarah.jpg
  - name: Savannah Sparks
    year: 2021
    scholarshipImage: 
  - name: Hannah Weeks
    year: 2021
    scholarshipImage: /uploads/weeks-hannah.jpg
  - name: James Drew
    year: 2020
    scholarshipImage: /uploads/drew-james.jpg
  - name: Geoffrey Dylan
    year: 2020
    scholarshipImage: /uploads/dylan-geoffrey.jpg
  - name: Kassandra Jenson
    year: 2020
    scholarshipImage: /uploads/jenson-kassandra.jpg
  - name: David Rodriguez
    year: 2020
    scholarshipImage: /uploads/rodriguez-david.jpg
  - name: Parker Uphoff
    year: 2020
    scholarshipImage: /uploads/uphoff-parker.jpg
  - name: Stephen Downie
    year: 2019
    scholarshipImage: /uploads/downie-stephen.jpg
  - name: Saville Feist
    year: 2019
    scholarshipImage: /uploads/feist-saville.jpg
  - name: Caleb Franssen
    year: 2019
    scholarshipImage: /uploads/franssen-caleb.jpg
  - name: Matthew LaFave
    year: 2019
    scholarshipImage: /uploads/lafave-matthew.jpg
  - name: Binqi Li
    year: 2019
    scholarshipImage: /uploads/li-maggie.jpg
  - name: Emmett Moore
    year: 2019
    scholarshipImage: 
  - name: Logan Scamfer
    year: 2019
    scholarshipImage: /uploads/scamfer-logan.jpg
  - name: Claire Severin
    year: 2019
    scholarshipImage: 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Recipients of these scholarships must exhibit strong science, technology, engineering and math (STEM) skills. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
