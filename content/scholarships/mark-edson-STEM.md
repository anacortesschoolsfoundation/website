---
title: Mark A. Edson Legacy Fund STEM Scholarship
est: 2021
areas: 
  - STEM
description: The Mark A. Edson Legacy Fund STEM Scholarship was established in 2020 by an anonymous donor through the Seattle Foundation.
keywords: Scholarship, Mark Edson scholarship, STEM scholarship
featuredImage: 
donorImage:
recipients:
  - name: Woojun Jeong
    year: 2021
    scholarshipImage: /uploads/jeong-woojun.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This award goes to a student seeking out an education in a STEM field. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
