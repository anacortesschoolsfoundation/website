---
title: Connor Wollam Memorial Scholarship
aliases:
    - /scholarships/scholarships-66/104-connor-wollam-memorial-scholarship.html
est: 2011
areas: 
  - Health and Human Services
  - Memorial
featuredImage: 
keywords: Anacortes School District, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, Chuck Starkovich, Kay Starkovich, starkovich scholarship
description: This award goes to a student pursuing a 2-year degree, a CTE path, vocational, or technical program.
donorImage:
recipients:
  - name: Adrien Garcia
    year: 2021
    scholarshipImage: /uploads/garcia-adrien.jpg
  - name: Benjamin Omdal
    year: 2020
    scholarshipImage: /uploads/omdal-benjamin.jpg
  - name: Giuliana (Mia) Oliver
    year: 2019
    scholarshipImage: /uploads/oliver-giuliana.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Connor Patrick Wollam developed A.L.L. leukemia at the age of two and underwent treatment at Seattle's Children's Hospital over the following five years, ending in a bone marrow transplant in 1999. Complications in the transplant took his life on Oct. 27, 1999, at the age of seven. Connor's last year was spent largely at the Ronald McDonald House in Seattle where, despite his suffering, he lived life with great love and passion for the people and things around him. The Connor Wollam Memorial Scholarship was first awarded in 2011, marking the year Connor would have graduated with his class. This scholarship is awarded to a graduating senior who intends to pursue a degree in health or social work and who has demonstrated concern for peers and others. The recipient must have shown character and passion in moving through his or her life in either overcoming obstacles or in movement toward their fellows.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

