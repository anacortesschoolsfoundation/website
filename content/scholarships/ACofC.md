---
title: Anacortes Chamber of Commerce Business Scholarship
est: 2023
areas: 
  - business
keywords: anacortes scholarship, entrepreneur, scholarship, scholarships, scholarship list, available scholarships, anacortes scholarships, 
description: The Anacortes Chamber of Commerce Business Scholarship assists a motivated student looking to start their own business and/or grow another business.
featuredImage: /uploads/donor-acofc.jpg
donorImage: /uploads/donor-acofc.jpg
recipients:
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Anacortes Chamber of Commerce is a member-driven, non-profit organization of businesses, professionals, and community stakeholders that provide leadership to improve Anacortes' businesses and our community. They provide business services to their members, form partnerships and alliances with appropriate entities, engage in legislative advocacy, participate in economic development activities, and initiate action and value-added programs for their members and the community at large. They consistently strive to fulfill their mission to promote economic prosperity through innovative leadership, advocacy, and collaborative community engagement. They also support workforce development and entrepreneurs, and would like their scholarship to assist a motivated student looking to start their own business and/or grow another business.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

