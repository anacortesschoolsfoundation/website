---
title: Temcov Foundation Scholarship
aliases:
    - /scholarships/scholarships-66/150-temcov-foundation-scholarship.html
est: 2017
areas: 
  - Memorial
  - Sports
  - Environmental
description: ASF awards scholarships to graduating seniors who plan to attend a trade school, 2-year, or 4-year school and who have demonstrated interest in one or more of the following areas; Cultural Understanding, Environment, Involvement in Sports.
keywords: scholarshp, Temcov Foundation, trade school scholarship
featuredImage: /uploads/donor-temcov.jpg
donorImage: /uploads/donor-temcov.jpg
recipients:
  - name: Alyssa Kiser
    year: 2021
    scholarshipImage: 
  - name: Kaitlin Lang
    year: 2021
    scholarshipImage: 
  - name: Lauren Long
    year: 2021
    scholarshipImage: 
  - name: Matthew Lowrie
    year: 2021
    scholarshipImage: /uploads/lowrie-matthew.jpg
  - name: Victoria Pittman
    year: 2021
    scholarshipImage: /uploads/pittman-victoria.jpg
  - name: Dominic Ermi
    year: 2020
    scholarshipImage: /uploads/ermi-dominic.jpg
  - name: Kalysta Mankins
    year: 2020
    scholarshipImage: /uploads/mankins-kalysta.jpg
  - name: Benjamin Marshall
    year: 2020
    scholarshipImage: /uploads/marshall-ben.jpg
  - name: Jennifer Spurling
    year: 2020
    scholarshipImage: /uploads/spurling-jennifer.jpg
  - name: Allyson Von Hagel
    year: 2020
    scholarshipImage: /uploads/von-hagel-allyson.jpg
  - name: Jonathan Askey
    year: 2019
    scholarshipImage: /uploads/askey-jonathan.jpg
  - name: Megan Bellusci
    year: 2019
    scholarshipImage: /uploads/bellusci-megan.jpg
  - name: Caleb Cross
    year: 2019
    scholarshipImage: /uploads/cross-caleb.jpg
  - name: Abigail Davis
    year: 2019
    scholarshipImage: /uploads/davis-abigail.jpg
  - name: Kailey Davis
    year: 2019
    scholarshipImage: /uploads/davis-kailey.jpg
  - name: Nhi Ngo
    year: 2019
    scholarshipImage: /uploads/ngo-nhi.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Temcov Foundation was established by John Temcov, a Bulgarian immigrant who came to the US at age 21 seeking a better education. During World War II, Temcov drove an ambulance at Fort Lewis, where he wrote in an Army newspaper about a new postwar world order based on a borderless society. After the war, he married and moved to West Seattle and for decades sold homes in the greater Seattle area. In the year before he died, John Temcov established the Temcov Foundation, which routinely supports numerous philanthropic causes, including schools, libraries, hospitals, museums and food/clothes banks. ASF awards scholarships to graduating seniors who plan to attend a trade school, 2-year, or 4-year school and who have demonstrated interest in one or more of the following areas; Cultural Understanding, Environment, Involvement in Sports. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
