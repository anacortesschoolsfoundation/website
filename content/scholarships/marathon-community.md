---
title: Marathon Community Scholarship
est: 2007
areas: 
  - STEM
  - Technical or Vocational
description: Marathon Petroleum supports many ASF programs and is a strong partner in STEM enrichment.
keywords: Marathon scholarship, STEM scholarship, Marathon STEM
featuredImage: /uploads/donor-marathon.jpg
donorImage: /uploads/donor-marathon.jpg
recipients:
  - name: Genevieve Kochel
    year: 2023
    scholarshipImage: /uploads/Marathon - Kochel, Genevieve.jpg
  - name: McKernan Boland
    year: 2023
    scholarshipImage: /uploads/Marathon - Boland, McKernan.jpg
  - name: Alexander Carroll
    year: 2022
    scholarshipImage: 
  - name: Chase Cornett
    year: 2022
    scholarshipImage: 
  - name: Henry Shaw
    year: 2022
    scholarshipImage: 
  - name: Ellie Goodwin
    year: 2022
    scholarshipImage: /uploads/Marathon - Goodwin, Ellie.PNG
  - name: Alyssa Jensen
    year: 2022
    scholarshipImage: /uploads/Marathon - Jensen, Alyssa.jpg
  - name: Chase Cornett
    year: 2021
    scholarshipImage: /uploads/cornett-chase.jpg
  - name: Jayden Frydenlund
    year: 2021
    scholarshipImage: /uploads/frydenlund-jayden.jpg
  - name: Ellie Goodwin
    year: 2021
    scholarshipImage: /uploads/goodwin-ellie.jpg
  - name: Kyler Martindale
    year: 2021
    scholarshipImage: /uploads/martindale-kyler.jpg
  - name: Evan Rodriguez
    year: 2021
    scholarshipImage: /uploads/rodriguez-evan.jpg
  - name: Aspen Eagle
    year: 2020
    scholarshipImage: /uploads/eagle-aspen.jpg
  - name: Garrett Kennedy
    year: 2020
    scholarshipImage: /uploads/kannedy-garrett.jpg
  - name: Quinn Millard
    year: 2020
    scholarshipImage: /uploads/millard-quinn.jpg
  - name: Cameron Rice
    year: 2020
    scholarshipImage: /uploads/rice-cameron.jpg
  - name: Evan Rodriguez
    year: 2020
    scholarshipImage: /uploads/rodriguez-evan.jpg
  - name: Eliot Briefer
    year: 2019
    scholarshipImage: 
  - name: Jayden Frydenlund
    year: 2019
    scholarshipImage: /uploads/frydenlund-jayden.jpg
  - name: Hannah Gleason
    year: 2019
    scholarshipImage: 
  - name: David Rodriguez
    year: 2019
    scholarshipImage: /uploads/rodriguez-david.jpg
  - name: Riley Ward
    year: 2019
    scholarshipImage: /uploads/ward-riley.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Each year the Anacortes Marathon Refinery awards scholarships to students attending an accredited university or community college and/or a technical or trade school. Scholarships support those majoring in engineering, sciences, or a trade skill in process technology, instrumentation and control systems, welding or machinery and must be used for academic pursuit in the upcoming school year. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

