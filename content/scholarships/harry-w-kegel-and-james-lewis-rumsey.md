---
title: Harry W Kegel and James Lewis Rumsey Scholarship
est: 2008
areas: 
  - STEM
  - Education
description: The Harry W Kegel and James Lewis Rumsey Family Endowment was created to fund this scholarship in honor of two men who understood the value of education and were important figures in Anacortes School District history.
keywords: scholarship, Harry Kegel scholarship, James Rumsey scholarship
featuredImage: 
donorImage:
recipients:
  - name: Gibson Groenig
    year: 2023
    scholarshipImage: /uploads/Kegel - Groenig, Gibson.jpg
  - name: Erik Dotzauer
    year: 2022
    scholarshipImage: 
  - name: Matthew Lujan
    year: 2021
    scholarshipImage:
  - name: Madelyn Hamblen
    year: 2020
    scholarshipImage: /uploads/hamblen-madelyn.jpg 
  - name: Alexander Havel
    year: 2019
    scholarshipImage: /uploads/havel-alexander.jpg
  - name: Molly Lavelle
    year: 2019
    scholarshipImage: /uploads/lavelle-molly.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The H.W. Kegel and J.L. Rumsey Scholarship was created by Arnold and Martha Kegel in honor of Arnold's father, Harry Kegel, and grandfather, James Rumsey. Both of these men understood the value of education and were important figures in Anacortes Schools' history. J.L. Rumsey was a businessman and served on the school board in the 1930's. He is depicted in two Anacortes Mural Project scenes downtown. His son-in-law, Harry, was a lifelong educator. Mr. Kegel taught junior and senior high school and in 1936 he became principal of the old Nelson School located at 29th and Commercial.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

The H.W. Kegel and J.L. Rumsey Scholarship was created by Arnold and Martha Kegel in honor of Arnold's father, Harry Kegel, and grandfather, James Rumsey. Both of these men understood the value of education and were important figures in Anacortes Schools' history. J.L. Rumsey was a businessman and served on the school board in the 1930's. He is depicted in two Anacortes Mural Project scenes downtown. His son-in-law, Harry, was a lifelong educator. Mr. Kegel taught junior and senior high school and in 1936 he became principal of the old Nelson School located at 29th and Commercial.