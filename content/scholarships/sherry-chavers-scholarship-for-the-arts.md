---
title: Sherry Chavers Scholarship for the Arts
aliases:
    - /scholarships/scholarships-66/180-sherry-chavers-scholarship-for-the-arts.html
areas: 
  - Arts
est: 2020
description: Sherry Chavers taught elementary school in Anacortes, sharing her passion for all things art.
keywords: scholarship, art scholarship, Sherry Chavers
featuredImage: /uploads/donor-chavers.jpg
donorImage: /uploads/donor-chavers.jpg
recipients:
  - name: Lucy Price
    year: 2023
    scholarshipImage: /uploads/Chavers - Price, Lucy.jpg
  - name: Lacey Stickles
    year: 2022
    scholarshipImage: /uploads/Sharry Chavers_ - Stickles, Lacey.JPG
  - name: Michael Hanrahan
    year: 2022
    scholarshipImage: /uploads/Sherry Chavers_ - Hanrahan, Michael-crop.jpg
  - name: Amy Aggergaard
    year: 2021
    scholarshipImage: /uploads/aggergaard-amy.jpg
  - name: Emilie Shjarback
    year: 2021
    scholarshipImage: /uploads/shjarback-emilie.jpg
  - name: Jacob Hoxie
    year: 2020
    scholarshipImage: /uploads/hoxie-jacob.jpg
  - name: Sadie Rich
    year: 2020
    scholarshipImage: /uploads/rich-sadie.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              For 38 years, Sherry Chavers taught elementary school in Anacortes, sharing her passion for all things art. As an arts advocate, Chavers sees great worth in students sharing their own art and voice with authentic audiences. Chavers, now a volunteer in the schools, works with all grade levels, helping students in drama, writing, poetry and visual art. She establishes this scholarship to assist a student who has demonstrated a passion for, talent and interest in, or commitment to some aspect of the "Arts" - to include Literary Arts (writing in any genre for different purposes, reading, speaking), Drama, the Visual Arts in all of its manifestations, Music, Dance, Graphic Arts. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

