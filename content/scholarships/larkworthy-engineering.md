---
title: Larkworthy Engineering Scholarship
est: 2011
areas: 
  - Engineering
description: This scholarship celebrates the recipient's accomplishments and ability to see the beauty of our world in a way known only to the engineers amongst us.
keywords: scholarship, engineering scholarship, engineer scholarship
featuredImage: /uploads/donor-larkworthy.jpg
donorImage: /uploads/donor-larkworthy.jpg
recipients:
  - name: Ellie Goodwin
    year: 2022
    scholarshipImage: /uploads/Larkworthy - Goodwin, Ellie.PNG
  - name: Ellie Goodwin
    year: 2021
    scholarshipImage: /uploads/goodwin-ellie.jpg
  - name: Binqi (Maggie) Li
    year: 2020
    scholarshipImage: /uploads/li-maggie.jpg
  - name: Binqi (Maggie) Li
    year: 2019
    scholarshipImage: /uploads/li-maggie-2019.jpg 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Larkworthy Engineering Scholarship is awarded to a student planning a career in Mechanical Engineering. Pete Larkworthy passed away in Dec.2011, but helped to establish this scholarship before his passing. Pete was born in Montreal, Canada, and graduated from the University of Winnipeg. His career in mechanical engineering started in the 1960's when he joined the team tasked with developing the "new" Boeing 747. Mountain climbing, skydiving, long distance bicycling, diving, running, skiing, and kayaking all were passions for Pete. Sailing, though, became his love, and he and his wife of 33 years, Linda Zielinski, spent 8 years building and 19 years sailing his beloved Marara—a very seaworthy 36- foot sailboat. Pete's love of the outdoors and zest for life never diminished as his struggles with Huntington's Disease began in the late 1990's. Surrounded by the love and support of so many, Pete's life stayed rich and full to the end.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

