---
title: Kiwanis Sunrisers Scholarship
aliases:
    - /scholarships-66/172-kiwanis-sunrisers-scholarship.htm
est: 1984
areas: 
  - Technical or Vocational
description: The Anacortes Kiwanis Sunrisers award a number of scholarships each year. 2021 will mark the 42nd consecutive year that Anacortes Sunrisers have awarded scholarships to graduates of Anacortes High School.
keywords: scholarships, Kiwanis Sunrisers, Kiwanis Sunrisers scholarship, Kiwanis technical scholarship, Kiwanis vocational scholarship
featuredImage: /uploads/donor-kiwanis-sunrisers.jpg
donorImage: /uploads/donor-kiwanis-sunrisers.jpg
recipients:
  - name: Juliette Scott
    year: 2023
    scholarshipImage: /uploads/Sunrisers - Scott, Juliette.jpeg
  - name: Matthew Rutz
    year: 2023
    scholarshipImage: /uploads/Sunrisers - Rutz, Matthew.jpg
  - name: Landen Frost
    year: 2023
    scholarshipImage: /uploads/Sunrisers - Frost, Landen.jpg
  - name: Alicia Ambriz-Espino
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Ambriz-Espino, Alicia.jpg
  - name: McKenzie Blouin
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Blouin, McKenzie.jpg
  - name: Arlene Cruz
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Cruz, Arlene.JPG
  - name: Livy Gates
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Gates, Livy.JPG
  - name: Ziming Lei Lin
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Lei Lin, Ziming.jpg
  - name: Guadalupe Ruiz
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Ruiz, Guadalupe.PNG
  - name: Alaina Schafer
    year: 2022
    scholarshipImage: /uploads/Kiwanis Sunrisers - Schafer, Alaina.jpg
  - name: Emma Bakke
    year: 2022
    scholarshipImage: /uploads/bakke-emma.jpg
  - name: Oswaldo Bonilla
    year: 2022
    scholarshipImage: 
  - name: Patrick Quinn
    year: 2022
    scholarshipImage: 
  - name: Emma Skidmore
    year: 2022
    scholarshipImage: 
  - name: Emma Bakke
    year: 2021
    scholarshipImage: /uploads/bakke-emma.jpg
  - name: Amelia Gegen
    year: 2021
    scholarshipImage: 
  - name: Makenna Goss
    year: 2021
    scholarshipImage: 
  - name: Lauren Harrison
    year: 2021
    scholarshipImage: 
  - name: Alyssa Jensen
    year: 2021
    scholarshipImage: /uploads/jensen-alyssa.jpg
  - name: Ziming Lin
    year: 2021
    scholarshipImage: /uploads/lin-ziming.jpg
  - name: Alaina Schafer
    year: 2021
    scholarshipImage: /uploads/schafer-alaina.jpg
  - name: Steven Sikic
    year: 2021
    scholarshipImage: 
  - name: Tyler Blouin
    year: 2020
    scholarshipImage: 
  - name: Sophie Flores
    year: 2020
    scholarshipImage: /uploads/flores-sophie.jpg
  - name: Jayden Frydenlund
    year: 2020
    scholarshipImage: /uploads/frydenlund-jayden.jpg
  - name: Jake Hightower
    year: 2020
    scholarshipImage: /uploads/hightower-jake.jpg
  - name: Kevin Sanchez Palacio
    year: 2020
    scholarshipImage: /uploads/palacio-sanchez-kevin.jpg
  - name: Evan Pies
    year: 2020
    scholarshipImage: /uploads/pies-evan.jpg
  - name: Cameron Rice
    year: 2020
    scholarshipImage: /uploads/rice-cameron.jpg
  - name: Jackson Andrew Tiessen
    year: 2020
    scholarshipImage: /uploads/tiessen-jackson.jpg
  - name: Cassandra Ausland
    year: 2019
    scholarshipImage: /uploads/ausland-cassandra.jpg
  - name: Spencer Baker
    year: 2019
    scholarshipImage: 
  - name: Jayden Frydenlund
    year: 2019
    scholarshipImage: /uploads/frydenlund-jayden.jpg
  - name: David Jonas
    year: 2019
    scholarshipImage: 
  - name: Dean Overman
    year: 2019
    scholarshipImage: 
  - name: Tiegan Peace
    year: 2019
    scholarshipImage: /uploads/peace-tiegan.jpg
  - name: Lyssa Petitclerc
    year: 2019
    scholarshipImage: /uploads/petitclerc-lyssa.jpg
  - name: Lexie Prue
    year: 2019
    scholarshipImage: /uploads/prue-lexie.jpg
  - name: Ryan Rubalcava
    year: 2019
    scholarshipImage: /uploads/rubalcava-ryan.jpg
  - name: Cort Senff
    year: 2019
    scholarshipImage: 
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Kiwanis Sunrisers scholarships are made possible by citizens and community partners who support the Kiwanis Sunriser's fundraising efforts, including sales of "Passport to Savings" coupon books, Christmas trees, and food wagon items. Scholarships are just one way Kiwanis fulfills its mission of "Serving the Children of the World." Kiwanis Sunrisers have been awarding scholarships to Anacortes students since 1984. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

 