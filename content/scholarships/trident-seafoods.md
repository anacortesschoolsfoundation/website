---
title: Trident Seafoods Scholarship
aliases:
    - /scholarships/scholarships-66/128-trident-seafoods-scholarship.html
est: 2015
areas: 
  - Technical or Vocational
description: This scholarship was initiated to aid students who are interested in becoming knowledgable and proficient in a trade without necessarily aquiring a four-year college degree. 
keywords: scholarship, Trident scholarship, trade school scholarship
featuredImage: /uploads/donor-trident.jpg
donorImage: /uploads/donor-trident.jpg
recipients:
  - name: Stephen Myers
    year: 2022
    scholarshipImage: 
  - name: Joshua Smeltzer
    year: 2021
    scholarshipImage: /uploads/smeltzer-joshua.jpg
  - name: Jake Hightower
    year: 2020
    scholarshipImage: /uploads/hightower-jake.jpg
  - name: Riley Ward
    year: 2019
    scholarshipImage: /uploads/ward-riley.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Trident Seafoods developed this program to aid students who wish to advance their education and may otherwise be unable to do so. The Trident Seafoods Scholarship is awarded to a graduating senior from AHS who is planning to attend a technical school, vocational school, or community college and who may be pursuing education or training for a career in the trade labor industry. Examples of career paths include, but are not limited to, Mechanic, Welder, Electrician, Electronics Technician, Carpenter, etc.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
