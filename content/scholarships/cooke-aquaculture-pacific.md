---
title: Cooke Aquaculture Pacific Scholarship
est: 2015
areas: 
  - Technical or Vocational
description: The Cooke Aquaculture scholarship is awarded to a graduating senior who plans to attend an accredited technical school, vocational school, or college and has demonstrated financial need.
keywords: scholarship, Cooke Aquaculture, vocational scholarship
featuredImage: /uploads/donor-cooke.jpg
donorImage: /uploads/donor-cooke.jpg
recipients:
  - name: Logan Hanrahan
    year: 2020
    scholarshipImage: /uploads/hanrahan-logan.jpg
  - name: Jayden Bates
    year: 2019
    scholarshipImage: /uploads/bates-jayden.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Cooke Aquaculture Pacific, part of the Cooke Aquaculture family of companies and formerly known as American Gold Seafoods, established this scholarship in 2015 to aid students who wish to advance their education and may otherwise be financially unable to do so. The scholarship is awarded to a graduating senior who plans to attend an accredited technical school, vocational school, or college and has demonstrated financial need.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

