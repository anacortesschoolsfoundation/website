---
title: Floyd Willette Scholarship
est: 2012
areas: 
  - Technical or Vocational
description: The Floyd Willette Scholarship was established to support a student who might otherwise not have a chance to attend post-secondary school education. 
keywords: scholarship, Floyd Willett scholarship
featuredImage: 
donorImage:
recipients:
  - name: Juliette Scott
    year: 2023
    scholarshipImage: /uploads/Willette - Scott, Juliette.jpeg
  - name: Arlene Cruz
    year: 2022
    scholarshipImage: /uploads/Floyd Willette - Cruz, Arlene.JPG
  - name: Jonah Umayam
    year: 2021
    scholarshipImage: /uploads/umayam-jonah.jpg
  - name: Eun-ha-su White
    year: 2020
    scholarshipImage: /uploads/white-eun-ha-su.jpg
  - name: Jayda Jameson
    year: 2019
    scholarshipImage: 
  - name: Torre Stotler
    year: 2019
    scholarshipImage: /uploads/stotler-torre.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Floyd A. Willett was born and raised in Anacortes. He graduated from AHS where he excelled in football and boxing, but his real passion was baseball. After enlisting in the Navy, he was stationed in San Diego and played baseball on the Navy team. Later, he was signed to the Seattle Rainier's farm team. Unfortunately, his dreams of a professional baseball career were cut short by a career-ending elbow injury. Floyd had the opportunity for a 4-year college degree, but family and life pre-empted that. He became a successful business owner in Anacortes. As a business owner, he understood the importance of higher education. 
              This award is given to a graduating senior who is a positive role model for peers, shows commitment and follow-through with activities and goals and plans to attend a 2-year community college, vocational or technical school in Washington.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

