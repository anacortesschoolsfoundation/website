---
title: Kathleen Dickinson Skagit Valley College Scholarship
est: 2020
areas: 
  - General
description: This scholarship is established by Kathleen Dickinson to assist a motivated, goal-driven student planning to attend Skagit Valley College, who either needs or merits help to achieve their goals.
keywords: scholarship, Kathleen Dickinson, Skagit Valley College scholarship
featuredImage: 
donorImage:
recipients:
  - name: Lizbeth Pena-Rangel
    year: 2022
    scholarshipImage: /uploads/Dickinson SVC - Pena-Rangel, Lizbeth.jpg
  - name: Kody Hargett
    year: 2020
    scholarshipImage: /uploads/hargett-kody.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              Student must have a GPA of 2.8 or higher, show some financial need and plan to attend Skagit Valley College.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
