---
title: Michael Jordan Memorial Scholarship
est: 2012
areas: 
  - Memorial
description: Michael Andrew Jordan's family would like to see this scholarship awarded to a graduating senior that possesses the same drive, ambition and level of academic excellence that Michael strived for throughout his brief life.
keywords: scholarship, Michael Jordan Scholarship, Jordan scholarship anacortes
featuredImage: 
donorImage:
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              This scholarship is established in memory of Michael Andrew Jordan, born on November 9, 1984.  At the age of 2 ½ Michael was diagnosed with Acute Lymphoblastic Leukemia (aka A.L.L). He had a bone marrow transplant in November, 1998, and spent the majority of his 8th Grade year in Seattle undergoing and recovering from the transplant surgery.  Despite his difficult health issues, he continued to focus on his education,  received tutoring from the Fred Hutchinson School, and maintained the academic excellence for which he strived. Michael Jordan was a member of the class of 2003. He was thrilled while, in remission, that he was able to return to AHS his freshman year and be immersed in the friendships he treasured so deeply.  Regretfully, Michael relapsed that summer and was only able to attend the first day of his sophomore year. Sadly, Michael died from leukemia on November 7, 2000, surrounded by family and friends. Michael was very gifted in imitating actors; he loved golf, video games and hanging out with family and friends.  He was smart, motivated, wildly entertaining, witty, charming and unabashedly honest. Michael demonstrated courage and resilience and unwavering faith and was inspired by his family and friends and members of the community during his valiant and courageous battle against his disease. It was Michael's desire to win his battle with cancer and go on to medical school and become a Pediatric Oncologist, much like the doctors that inspired him and treated him for his disease. His family would like to see this scholarship awarded to a graduating senior that possesses the same drive, ambition and level of academic excellence that Michael strived for throughout his brief life. 
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---

