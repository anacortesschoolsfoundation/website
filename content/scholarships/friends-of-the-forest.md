---
title: Friends of the Forest Scholarship
aliases:
    - /scholarships/scholarships-66/79-friends-of-the-anacortes-community-forest-lands-scholarship.html
est: 2011
areas: 
  - Environmental
description: Friends of the Forest Scholarships are awarded to students who have enthusiasm and a desire to be involved in the natural environment and who support Friends of the Forest's mission of environmental education, stewardship and outreach.
keywords: scholarship, ACFL, Anacortes Community Forest Lands, Friends of the Forest scholarship
featuredImage: /uploads/donor-acfl.jpg
donorImage: /uploads/donor-acfl.jpg
recipients:
  - name: Thomas Harris
    year: 2023
    scholarshipImage: /uploads/FoF - Harris, Thomas.jpeg
  - name: Saville Feist
    year: 2023
    scholarshipImage: /uploads/FoF - Feist, Saville.jpg
  - name: Vincent Jackson
    year: 2022
    scholarshipImage: 
  - name: Mia McCutcheon
    year: 2022
    scholarshipImage: 
  - name: Caitlin Brar
    year: 2022
    scholarshipImage: /uploads/Friends of the Forest - Brar, Caitlin.jpg
  - name: Kathryn Kamp
    year: 2021
    scholarshipImage: /uploads/kamp-kathryn.jpg
  - name: Katie Parker
    year: 2021
    scholarshipImage:
  - name: Jayden Zaharris
    year: 2021
    scholarshipImage: /uploads/zaharris-jayden.jpg
  - name: Kailey Davis
    year: 2020
    scholarshipImage: /uploads/davis-kailey.jpg
  - name: Cali Swain
    year: 2020
    scholarshipImage: /uploads/swain-cali.jpg
  - name: Grace Tesch
    year: 2020
    scholarshipImage: /uploads/tesch-grace.jpg
scholarship: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-scholarship
            
            content: |
              The Friends of the Forest Scholarships were originally established in 2009 by the Friends of the Forest; a non-profit group dedicated to the preservation of the Anacortes Community Forest Lands through education, outreach and stewardship. Friends formed in 1987 to "preserve, protect and enjoy" our community forest. Since that time, Friends has been instrumental in community forest awareness programs. Each year the organization provides a wide range of educational activities that promote a greater understanding of our forest environment. Friends of the Forest Scholarships are awarded to students who have enthusiasm and a desire to be involved in the natural environment and who support Friends of the Forest's mission of environmental education, stewardship and outreach. Preference is given to students who plan to study areas related to the outdoors, environment, conservation or nature.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: important-dates
---
