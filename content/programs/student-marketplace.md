---
title: "Celebrate the Season Student Marketplace"
aliases:
- /student
description: "Anacortes Schools Foundation presents Celebrate the Season's Holiday Fair!"
keywords: "Anacortes Schools, Anacortes School District, Celebrate the season, anacortes port, anacortes school fundraiser, anacortes schools foundation, anacortes school foundation fundraiser, anacortes auction, anacortes school auction, art contest, anacortes art contest, anacortes school art contest, anacortes foundation dinner, anacortes school celebration, school foundation fundraiser anacortes, anacortes district fundraiser, celebrate anacortes, anacortes school district, anacortes stem grants, anacortes scholarship fund fundraiser, anacortes celebration, ASD fundraiser, ASF Fundraiser"
featuredImage: /uploads/holiday-fair-img-01.png
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 1
    sectionclass: bg-ctswhite max-w-none !py-0 bg-fixed
    columns:
      - name: Main
        col_span: 1
        divclass: "!space-y-0"
        backgroundImage:
        column_components:
          - _bookshop_name: content-complex
            title:
            sectionclass:
            content: |
              {{< asf-figure title="Anacortes Schools Foundation Presents the Celebrate the Season Holiday Fair" divclass="mx-auto" figclass="w-full mx-auto" imgclass="mx-auto" capclass="" src="/uploads/2022-CTS-marketplace-header-r.gif" width=800 link="" >}}

              ## Students, teams, and clubs:
              {.text-center .font-aleo .text-hfgreen .max-w-xl .mx-auto}

              ### You're invited to sell your crafts, gifts, packaged foods, or fundraiser items at the Celebrate the Season Holiday Fair Student Marketplace!
              {.text-center .font-aleo .text-hfgreen .max-w-xl .mx-auto}

              <span class="text-ctsred">When:</span> Saturday, Dec. 10, 2022 | 11 AM to 4 PM  
              <span class="text-ctsred">Where:</span> <span class="text-ultraviolet not-prose ">[<i class="icon-location"></i> Port of Anacortes Transit Shed](https://goo.gl/maps/tPG1vvVDf3WMuj1UA)</span>  
              <span class="text-ctsred">Details: </span>Open to students, student teams and/or clubs. We will provide a 6' table and you bring the goods and sell. (Don't forget to bring some change!)  
              <span class="text-ctsred">Registration Deadline:</span> Dec. 2, 2022 (Space is limited!)  
              <span class="text-ctsred">Cost:</span> FREE! Click the link below to sign up
              {.text-left .font-aleo .text-hfgreen .max-w-xl .mx-auto .pt-8}

              <a href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2022HolidayFairStudentMarketplaceRegistrationForm" class="btn w-full">Sign up</a>
              {.max-w-3xl .mx-auto .text-center}

              Celebrate the Season Holiday Fair is brought to you by:
              {.pt-8 .max-w-3xl .mx-auto .text-hfgreen .text-xl .text-center .font-aleo}

              <div class="pb-8 text-center flex flex-col md:flex-row items-center max-w-full mx-auto">
                {{< asf-figure title="December 10, 2022 - 11 am to 4:00 pm" divclass="mx-auto order-1 md:order-2" figclass="w-full mx-auto" imgclass="mx-auto" capclass="" src="/uploads/ASF-logo-green-09.png" width=400 link="" >}}

                {{< asf-figure title="December 10, 2022 - 11 am to 4:00 pm" divclass="mx-auto order-2 md:order-1" figclass="w-full mx-auto" imgclass="mx-auto" capclass="" src="/uploads/2022-damon-tree-left-crop.png" width=400 link="" >}}

                {{< asf-figure title="December 10, 2022 - 11 am to 4:00 pm" divclass="mx-auto order-3 md:order-3" figclass="w-full mx-auto" imgclass="mx-auto" capclass="" src="/uploads/2022-playhouse-tree-right-crop.png" width=400 link="" >}}
              </div>
---
