---
title: Fueling Education Fun Run
aliases:
  - /events/fueling-education-fun-run.html
  - /news/144-successful-fun-run-supports-anacortes-schools-foundation.html
  - /news/155-island-view-elementary-receives-5-000-check.html
  - /funrun
bannerImage: /uploads/page-title-run.jpg
description: >-
  Every September, ASF partners with Puget Sound Refinery and races into the
  school year at the annual Fueling Education Fun Run.
keywords: >-
  Anacortes Schools, Anacortes School District, anacortes school fundraiser,
  anacortes 5k, anacortes fun run, anacortes shell run, anacortes school
  foundation fundraiser, ASD fundraiser, ASF Fundraiser
featuredImage: /uploads/2021-FR5k-1.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Fueling Education Fun Run
    bannerImage: /uploads/page-title-run-lower-r.jpg
  - _bookshop_name: columns
    num_cols: 4
    columns:
      - name: Logo
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            content: >
              {{< asf-figure title="2023 Fueling Education Fun Run"
              divclass="mx-auto w-full" figclass="w-full mx-auto"
              imgclass="mx-auto" capclass=""
              src="/uploads/HFSinclair_FunRun2023_FinalLogo (2).png" width=300
              link="" >}}
      - name: Text
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content-complex
            content: |
              <div class="flex flex-col items-center">
                <div class="text-asfpurple text-3xl font-bold">On your mark,</div>
                <div class="text-asfpurple text-3xl font-bold">get set,</div>
                <div class="text-asfpurple text-3xl font-bold">GO!!!</div>
                <div class="text-blackenedpearl text-2xl pt-4">We'll see you </div>
                <div class="text-blackenedpearl text-2xl">September 15, 2024</div>
                <div class="text-blackenedpearl text-2xl">at Washington Park!</div>
              </div>
      - name: Video
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading:
            youtubeID: NxO79bUwEy0
            text:
            divclass:
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width:
            height:
            images:
              - src: /uploads/IMG_7526.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7598.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7503.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7572.jpg
                title: Fueling Education 5k & Fun Run
      - name: Text
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              In September, ASF partners with [HF
              Sinclair](https://www.hfsinclair.com/operations/facilities/Puget-Sound-Refinery/default.aspx)
              and races into the school year at the annual Fueling Education Fun
              Run.


              HF Sinclair puts on this amazing event which brings our community
              together and helps raise close to $60,000 a year to support
              Anacortes students.


              In previous years, funds raised supported COVID Relief Grants
              (2020), mental health supports for Anacortes students (2021), STEM
              enrichment and CTE programming (2022), 3rd Grade Swim Program
              (2023) and STEM College Scholarships.


              Stay tuned for information on the 2024 Fun Run


              We'd love to see your race day pictures! Email them to
              [asf4kids@gmail.com](mailto:asf4kids@gmail.com) or share them on
              social media with the privacy set to public and use the hashtag
              **\#DINOFunRun2023** to join in on the fun!


              <a class="btn"
              href="https://www.databarevents.com/fuelingeducation">Register</a>
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            title: We can't do this without you!
            text: >-
              We would love to have YOUR HELP at this year's event and there are
              plenty of ways you can participate!
            steps:
              - heading: Make a Donation
                text: Help support our event with a financial donation.
                action: Donate
                url: /giving
              - heading: Be a Sponsor
                text: >-
                  Support education while promoting your business. Fill out the
                  [Sponsor
                  Form](https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023FuelingEducationFunRunSponsorshipForm)
                  and contact [marta@asfkids.org](mailto:Marta@asfkids.org) for
                  more information.
                action: Learn More
                url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023FuelingEducationFunRunSponsorshipForm
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width:
            height: 300
            images:
              - src: /uploads/2022-FEFR-IMG_7444.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7610.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7613.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7615.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7617.jpg
                title: Fueling Education 5k & Fun Run
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width: 600
            height:
            images:
              - src: /uploads/IMG_7472.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7771.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7776.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_7953.jpg
                title: Fueling Education 5k & Fun Run
              - src: /uploads/IMG_8329.jpg
                title: Fueling Education 5k & Fun Run
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >
              Every year, HF Sinclair provides a $5,000 grant to the ASD school
              with the highest percentage of their student body that
              participates in this race. All ASD staff and students are eligible
              to participate in this competition to help their school win
              $5,000! Last year it was a tight competition, and we want to make
              sure we count all eligible racers, so the winning school will be
              announced by ASF on social media the week after the race. Follow
              us on [Facebook](https://www.facebook.com/asfkids) and
              [Instagram](https://www.instagram.com/asf4kids/) to be the first
              to find out who the winner is!


              In addition to the ASD Grant, Race Day Prizes will include:


              * Awards will be given to 1st, 2nd, and 3rd Place – Male & Female
              in the race.

              * An Award will be given to the fastest Teacher in Anacortes
              School District.

              * An Award will be given to the fastest HF Sinclair Anacortes
              Refinery employee.

              * **New 2023:** An Award will be given to the fastest person who
              is a Sponsor of the Race

              * Trophies will be given to the fastest child in each age category
              for the kids race.

              * Everyone will receive a finisher medal!

              * There will be raffle prizes randomly given out Day of Race.


              Race Director: James Steller - (360) 293-1766 -
              [James.Steller@HFSinclair.com](mailto:James.Steller@HFSinclair.com).


              Questions about race sponsorship? Contact Marta McClintock via
              phone (360) 488-2343 or email
              [marta@asfkids.org](mailto:marta@asfkids.org).
  - _bookshop_name: heading
    title: >-
      Thank you to everyone that supported the 8th annual Fueling Education Fun
      Run!
    subtitle:
    text: >-
      Check out pictures of this amazing day in our photo album by clicking the
      button below. The passcode is **4396**. Enjoy!
    url: >-
      https://karladecampphotography.pixieset.com/anacortesfuelingeducationfunrun
    action: View Album
    sectionclass: items-center text-center max-w-4xl
  - _bookshop_name: marquee
    width: 200
    message:
      heading: Thank You ASF Annual Sponsors!
      text: >-
        We are so grateful to our many community organizations that sponsor ASF
        events.
      url: /giving/sponsor/
    images:
      - name: Athlon
        url: https://athlonsolutions.com/
        image: /uploads/Sponsors_athlon.jpg
      - name: Anacortes School District
        url: https://www.asd103.org/
        image: /uploads/Sponsors_ASD.jpg
      - name: Central Welding Supply
        url: https://www.centralwelding.com/
        image: /uploads/Sponsors_central-welding-supply.jpg
      - name: Envirogreen Technologies
        url: https://envirogreentech.com/
        image: /uploads/Sponsors_envirogreen.jpg
      - name: WR Grace
        url: https://grace.com/
        image: /uploads/Sponsors_grace.jpg
      - name: Skagit Publishing
        url: https://www.goskagit.com/
        image: /uploads/Sponsors_skagit-publishing.jpg
      - name: Swinomish Casino
        url: https://www.swinomishcasinoandlodge.com/
        image: /uploads/Sponsors_swinomish.jpg
      - name: Fisher Scientific
        url: https://www.fishersci.com/us/en/home.html/
        image: /uploads/Sponsors_fisherscientific.jpg
      - name: Whatcom Environmental
        url: https://whatcom-es.com/
        image: /uploads/Sponsors_whatcomenvironmental.jpg
      - name: IRG
        url: https://www.irgpt.com/
        image: /uploads/Sponsors_irg.jpg
      - name: MPe
        url: https://mpenviro.com/
        image: /uploads/Sponsors_mpe.jpg
      - name: City of Anacortes
        url: https://www.anacorteswa.gov/
        image: /uploads/Sponsors_cityofanacortes.jpg
      - name: HF Sinclair
        url: https://www.hfsinclair.com/operations/facilities/Puget-Sound-Refinery/
        image: /uploads/Sponsors_hfsinclair.png
      - name: PowerTek
        url: https://www.powertek.net/
        image: /uploads/Sponsors_powertek.jpg
      - name: ThermoFisher Scientific
        url: https://www.thermofisher.com/us/en/home.html
        image: /uploads/Sponsors_thermofisher.jpg
      - name: Veca
        url: https://www.veca.com/
        image: /uploads/Sponsors_veca.jpg
      - name: Western Refinery
        url: https://wrsweb.com/
        image: /uploads/Sponsors_wrs.jpg
    speed: 20000
    height: 100
---
