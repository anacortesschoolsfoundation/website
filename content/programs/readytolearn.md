---
title: Ready to Learn Fair
aliases:
  - /ready
  - /ready-to-learn-fair.html
bannerImage: /uploads/page-title-rtlf-2022.jpg
description: >-
  Each year, ASF provides back-to-school essentials for Anacortes families who
  need a little extra help. Families receive a new FREE backpack full of
  supplies tailored to their child(ren)'s specific year in school (K-12).
keywords: >-
  Anacortes Schools, Anacortes School District, ready to learn fair, ready to
  learn, school supplies, anacortes school supplies, anacortes school district
  school supplies list, anacortes school district supplies, where can I get free
  school supplies
featuredImage: /uploads/prog-rtl3.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Ready to Learn Fair
    bannerImage: /uploads/page-title-rtlf-2022.jpg
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Colorful Text
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content-complex
            title:
            content: >
              <div class="not-prose">

              {{< asf-figure title="ASF Board Members" caption="" divclass=""
              figclass="items-center" imgclass=""
              src="/uploads/rtlf-rainbow-top.jpg" width=630 >}}

              {{< asf-figure title="ASF Board Members" caption="" divclass="p-4"
              figclass="items-center" imgclass="" src="/uploads/rtlf-logo.jpg"
              width=630 >}}

              <div class="flex flex-col items-center space-y-2 text-xl italic
              font-bold font-serif">
                <span style="color: #ff2323;">Saturday, August 19, 2023</span>
                <span style="color: #ff9451;">9am-12pm</span>
                <span style="color: #5bbb6c;">Location: Anacortes High School</span>
                <span style="color: #4678b4;">Registration opens August 1, 2023</span>
                <div class="flex flex-row space-x-4">
                  <a class="btn" name="Register in English" href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairRegistrationForm">Register (English)</a>
                  <a class="btn" name="Registrarse en Español" href="https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairFormularioDeInscripción">Registrarse (Español)</a>
                </div>
              </div>

              {{< asf-figure title="ASF Board Members" caption="" divclass=""
              figclass="items-center" imgclass=""
              src="/uploads/rtlf-rainbow-lower.jpg" width=630 >}}

              </div>
      - name: Video
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading:
            youtubeID: 7H2VB61n-3c
            text: >-
              The annual Ready to Learn Fair helps kids start the year feeling
              proud! ASF and community partners provide FREE back-to-school
              essentials including backpacks and supplies, books, gently used
              clothing, haircuts, new underwear and socks,  resources, and
              information. A team of dedicated volunteers puts the fair on with
              the support of the Anacortes School District and funding from ASF.
            divclass:
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            title: We need your help!
            title_url: https://www.databarevents.com/fuelingeducation
            text: >-
              We would love to have YOUR HELP at this year's event and there are
              plenty of ways you can participate!
            steps:
              - heading: Make a Donation
                heading_url: /giving
                url: https://www.databarevents.com/fuelingeducation
                text: >-
                  For just under $30 each, ASF provides students with a new
                  backpack and an assortment of necessary, grade-level
                  appropriate school supplies. To meet this year's critical
                  needs, over $18,000 and the support of many volunteers will be
                  needed to make the 2023 Ready to Learn Fair a success. We
                  invite you to support this important effort in any way you
                  can.
                action: Donate
                action_url: /giving
              - heading: Be a Sponsor
                heading_url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairSponsorshipForm
                url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairSponsorshipForm
                text: >-
                  Support education while promoting your business. Fill out the
                  [Sponsor
                  Form](https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairSponsorshipForm)
                  and contact
                  [president@asfkids.org](mailto:president@asfkids.org) for more
                  information.
                action: Learn More
                action_url: >-
                  https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023ReadyToLearnFairSponsorshipForm
              - heading: Volunteer
                url: /volunteer
                heading_url: /volunteer
                text: >-
                  The annual Ready to Learn Fair has been an essential service
                  for Anacortes students for over 20 years and it wouldn't be
                  possible without our amazing volunteers! If you'd like to
                  volunteer at this year's Ready to Learn Fair, we'd love to
                  have your help! Click the Volunteer button to complete our
                  Volunteer Interest Form and we'll be in touch!
                action: Volunteer
                action_url: /volunteer
      - name: Slides
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: slides
            width: 650
            height:
            images:
              - src: /uploads/rtl-2023-5.jpg
                title: Donate, Sponsor and Volunteer
              - src: /uploads/bottom carousel-500.jpg
                title: Donate, Sponsor and Volunteer
              - src: /uploads/bottom carousel 3-500.jpg
                title: Donate, Sponsor and Volunteer
              - src: /uploads/bottom carousel 2-500.jpg
                title: Donate, Sponsor and Volunteer
  - _bookshop_name: media-text
    heading: Questions?
    text: >
      Ready to Learn Fair Committee Chair: Treva King at
      [tking@asd103.org](mailto:tking@asd103.org)

      Questions about volunteering? Contact Volunteer and Registration lead
      Rachel Esposito at [asf4kids@gmail.com.](mailto:asf4kids@gmail.com)
    image_right: false
    images:
      - title: Ready to Learn
        src: /uploads/rtl-2023-5.jpg
      - title: Ready to Learn
        src: /uploads/rtl-2023-6.jpg
      - title: Ready to Learn
        src: /uploads/rtl-4.jpg
      - title: Ready to Learn
        src: /uploads/rtl-6.jpg
    video: {}
    url: readytolearn
    action: Learn More
  - _bookshop_name: marquee
    width: 200
    message:
      heading: >-
        2022 Ready to Learn Fair was made possible in part by our generous
        sponsors.
      text: >-
        Additional thanks to many individuals, organizations and businesses who
        made donations to support this important event.
      url:
    images:
      - name: Fidalgo Island Rotary
        url: https://fidalgorotary.org/
        image: /uploads/Sponsors_fi-rotary.jpg
      - name: Anacortes Rotary
        url: https://anacortesrotary.org/
        image: /uploads/Sponsors_anacortes-rotary.jpg
      - name: Strandberg
        url: https://strandbergconstruction.com/
        image: /uploads/Sponsors_strandberg.jpg
      - name: Kiwanis Sunrisers
        url: https://www.facebook.com/AnacortesKiwanis/
        image: /uploads/Sponsors_kiwanis-sunrisers.jpg
      - name: Kiwanis Noon Kiwanis Club
        url: http://www.anacorteskiwanis.org/
        image: /uploads/Sponsors_kiwanis-noon.jpg
    sectionclass: items-center
    height: 100
    speed: 10000
---
