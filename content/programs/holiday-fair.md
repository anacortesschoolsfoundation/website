---
title: Celebrate the Season Holiday Fair
aliases:
  - /fair2022
  - /fair
  - /holiday
  - /holidayfair
  - /programs/holiday-fair-2022
description: Anacortes Schools Foundation presents Celebrate the Season's Holiday Fair!
keywords: >-
  Anacortes Schools, Anacortes School District, Celebrate the season, anacortes
  port, anacortes school fundraiser, anacortes schools foundation, anacortes
  school foundation fundraiser, anacortes auction, anacortes school auction, art
  contest, anacortes art contest, anacortes school art contest, anacortes
  foundation dinner, anacortes school celebration, school foundation fundraiser
  anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
  school district, anacortes stem grants, anacortes scholarship fund fundraiser,
  anacortes celebration, ASD fundraiser, ASF Fundraiser
featuredImage: /uploads/hff-featured-image.png
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 1
    sectionclass: >-
      bg-[url('/uploads/HFF2023-Top-bg-sm.png')] max-w-none !py-0 bg-scroll
      bg-cover background-repeat:no-repeat
    columns:
      - name: Logo
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: content-complex
            title:
            sectionclass: bg-ctswhite bg-opacity-0
            content: >
              {{< asf-figure title="Holiday Fair" divclass="mx-auto mt-8" 
              figclass="w-full mx-auto" imgclass="mx-auto" capclass=""
              src="/uploads/2023HFF-logo-pt.png" width=600 link="" >}}


              <div class="pt-8 text-center grid grid-cols-1 md:grid-cols-2 gap-8
              max-w-full mx-auto">
                <div>

                *Saturday*  
                9 DEC | 11-3 PM | FREE ENTRY  
                {.font-aleo .text-white .not-prose .text-xl .text-center .mt-4}

                Live Entertainment  
                Free Photos with Santa  
                Student Marketplace  
                Free Arts and Crafts  
                <span class="text-base">(while supplies last)</span>  
                {.font-aleo .text-white .not-prose .text-xl .text-center .mt-4}

                </div>
                <div>
                
                *Sunday*  
                10 DEC | 11-3 PM | FREE ENTRY  
                {.font-aleo .text-white .not-prose .text-xl .text-center .mt-4}
                
                Fidalgo Island Rotary  
                Gingerbread Decorating Contest  
                Doors open at 11:30 AM  
                Contest 12:00-2:00 PM  
                Judging to follow  
                {.font-aleo .text-white .not-prose .text-xl .text-center .mt-4}

                </div>
              </div>

              <span class="font-aleo text-white not-prose text-xl text-center
              block my-16 mx-auto"> Celebrate the Season Holiday Fair is brought
              to you by:</span>
          - _bookshop_name: content
            sectionclass: m-0 p-0 border-0
            title:
            content: >-
              {{< asf-figure title="Sponsored by Derek Damon Orthodontics,
              Playhouse Dental, Rotary Fidalgo Island and Anacortes Schools
              Foundation" src="/uploads/HFF-Mobile-sponsors-noborder.png"
              width=300 divclass="block md:hidden mx-auto" figclass="w-full
              mx-auto" imgclass="mx-auto" id="mobile" >}}


              {{< asf-figure title="Sponsored by Derek Damon Orthodontics,
              Playhouse Dental, Rotary Fidalgo Island and Anacortes Schools
              Foundation" src="/uploads/Sponsored-by-noborder.png" width=1280
              divclass="hidden md:block mx-auto" figclass="w-full mx-auto"
              imgclass="mx-auto" >}}
  - _bookshop_name: color-bar
    class: bg-ocean h-12
---
