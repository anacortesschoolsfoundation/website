---
title: "Become a Sponsor of Celebrate the Season"
aliases:
  - /sponsor
description: ASF remains committed to its mission of empowering all Anacortes students through scholarships and enriched learning opportunities, with our first priority for the 2021-2022 school year focused on Mental Health supports for Anacortes students. Please join us as a sponsor and help us address Mental Health needs in our schools.
keywords: sponsor celebrate the season, Anacortes Schools, Anacortes School District, Celebrate the season, anacortes port, anacortes school fundraiser, anacortes schools foundation, anacortes school foundation fundraiser, anacortes auction, anacortes school auction, art contest, anacortes art contest, anacortes school art contest, anacortes foundation dinner, anacortes school celebration, school foundation fundraiser anacortes, anacortes district fundraiser, celebrate anacortes, anacortes school district, anacortes stem grants, anacortes scholarship fund fundraiser, anacortes celebration, ASD fundraiser, ASF Fundraiser
featuredImage: /uploads/CTS-ASF-01.jpg
layout: blocks
content_blocks:
  - _bookshop_name: cognito
    key: UyfjasXdoU6KNNOYinFa-w
    number: 13
---