---
title: Celebrate the Season
aliases:
  - /events/holiday-family-fair.html
  - /holiday-fair-2021/
  - /celebrate2021
  - /celebrate2022
  - /holiday-fair-2022
  - /celebrate
  - /events/celebrate-the-season.html
description: >-
  Join us for a holiday event hosted by ASF! Celebrate the Season is a series of
  holiday events at the Port of Anacortes where hundreds of elves create a
  magical winter wonderland complete with Santa's House, Mrs. Claus's Kitchen,
  beautifully decorated trees and thousands of lights.
keywords: >-
  Anacortes Schools, Anacortes School District, Celebrate the season, anacortes
  port, anacortes school fundraiser, anacortes schools foundation, anacortes
  school foundation fundraiser, anacortes auction, anacortes school auction, art
  contest, anacortes art contest, anacortes school art contest, anacortes
  foundation dinner, anacortes school celebration, school foundation fundraiser
  anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
  school district, anacortes stem grants, anacortes scholarship fund fundraiser,
  anacortes celebration, ASD fundraiser, ASF Fundraiser
featuredImage: /uploads/CTS-ASF-01.jpg
layout: blocks
mainclass:
content_blocks:
  - _bookshop_name: content-complex
    title:
    content: >
      {{< asf-figure title="Celebrate the Season Logo - Mobile" divclass="block
      md:hidden mx-auto" figclass="w-full mx-auto" imgclass="mx-auto"
      capclass="" src="/uploads/CTS-2023-Logo-light-center2.png" width=400
      link="" >}}


      {{< asf-figure title="Celebrate the Season Logo" divclass="hidden md:block
      mx-auto" figclass="w-full mx-auto" imgclass="mx-auto" capclass=""
      src="/uploads/CTS-2023-Logo-light-center2.png" width=600 link="" >}}


      <div class="grid grid-cols-1 md:grid-cols-2 max-w-3xl mx-auto">
        <div class="text-center">

        ## Celebrate the Season Gala{.font-caps .text-lessmoody}

        **December 2, 2023**
        
        
        Thank you for joining us and we look forward to seeing you again next year!

        ## Holiday Family Fair{.font-aleo .text-blue-950}

        **December 9-10, 2023**
        </div>
      </div>



      <div class="max-w-3xl mx-auto text-justify mt-8">

      Each December, ASF hosts Celebrate the Season, a series of holiday events
      at the Port of Anacortes where hundreds of elves create a magical winter
      wonderland. We hope you will join us for our Gala Dinner, participate in
      our Online Auction and bring your kids to the annual Holiday Fair!
    sectionclass: max-w-full py-12 text-center
  - _bookshop_name: color-bar
    class: bg-moodyblue h-12
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Logo
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              {{< asf-figure title="Celebrate the Season Header Mobile Logo"
              src="/uploads/CTS2023Logo.png" width=400 divclass="block md:hidden
              mx-auto" figclass="w-full mx-auto" imgclass="mx-auto" >}}


              {{< asf-figure title="Celebrate the Season Header Logo"
              src="/uploads/CTS2023Logo.png" width=600 divclass="hidden md:block
              mx-auto" figclass="w-full mx-auto" imgclass="mx-auto" >}}
            sectionclass: bg-moodyblue bg-opacity-90
          - _bookshop_name: media-text
            heading: Gala Fundraiser Dinner
            text: >-
              The Celebrate the Season Gala Fundraiser Dinner is ASF's biggest
              fundraiser of the year and features an online and live auction,
              live entertainment, an exquisite dinner and our Raise the Paddle.
              Tickets go on sale November 4, 2023.
            image_right: false
            images:
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-001.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2022-cts-01.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2022-cts-02.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2022-cts-03.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-1.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-3.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-4.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps text-ctsgold
            textclass: text-white
            actionclass:
          - _bookshop_name: media-text
            heading:
            text: >
              The need for the 2023-24 school year is truly greater than ever.
              We know that students are suffering from serious depression,
              anxiety and other mental health issues and we are proud to be one
              of the only school foundations in the state to fund mental health
              at every grade level. Because mental health continues to be one of
              our most critical needs, ASF will once again be raising the paddle
              to support social workers at AMS, AHS and Cap Sante, a mental
              health therapist shared by all three elementary schools and, new
              this year, we are excited to maximize donor dollars by partnering
              with Communities in Schools.
            image_right: true
            images:
              - title: Celebrate the Season Gala
                src: /uploads/2022-cts-05.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-6.jpg
              - title: Celebrate the Season Gala
                src: /uploads/CTS-check.jpg
              - title: Celebrate the Season Gala
                src: /uploads/CTS-registration.jpg
              - title: Celebrate the Season Gala
                src: /uploads/CTS-Curtis.jpg
              - title: Celebrate the Season Gala
                src: /uploads/CTS-Chavers-Irish.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps
            textclass: text-white text-orange-100
            actionclass:
          - _bookshop_name: media-text
            heading:
            text: >
              We are celebrating this year with a nautical theme and a nod to
              our beautiful seaside town and our final year at the Port. Please
              consider joining us as a <a href="/giving/sponsor/"><span
              style="color: #9ed0f0">2023 Celebrate the Season
              sponsor</span></a> and helping ASF provide an anchor in the storm
              that is mental health.
            image_right: false
            images:
              - title: Celebrate the Season Gala
                src: /uploads/2022-cts-06.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-12.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-13.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-14.jpg
              - title: Celebrate the Season Gala
                src: /uploads/2021-cts-16.jpg
            video: {}
            url:
            action:
            sectionclass: bg-moodyblue bg-opacity-90 2xl:px-24
            headingclass: font-caps
            textclass: text-white text-orange-100
            actionclass:
    sectionclass: >-
      bg-[url('/uploads/sparkle-bg.png')] max-w-none !py-0 bg-fixed bg-no-repeat
      bg-cover
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Thank you!
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: media-text
            heading: Thank you for your extraordinary support!
            text: >-
              Anacortes Schools Foundation would like to extend a huge thank you
              to our community for making "Celebrate the Season" a huge success
              each year!
            image_right: true
            images:
              - title:
                src:
                width:
                link:
                caption:
            video:
              heading:
              divclass:
              youtubeID: uUzYyo8ffGw
              text:
            url:
            action:
            sectionclass:
            headingclass:
            textclass:
            actionclass:
          - _bookshop_name: sponsors-grid
            title: Thank you to our generous sponsors of 2023 Celebrate the Season
            filename: sponsors-cts-2022
            headingclass: text-asfpurple font-caps
    sectionclass: max-w-none !py-0
  - _bookshop_name: color-bar
    class: bg-lessmoody h-12
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Logo
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >
              {{< asf-figure title="Holiday Family Fair Mobile Logo"
              divclass="block md:hidden mx-auto" figclass="w-full mx-auto"
              imgclass="mx-auto" capclass="" src="/uploads/2023HFF-logo.png"
              width=300 link="" >}}

              {{< asf-figure title="Holiday Family Fair Logo" divclass="hidden
              md:block mx-auto" figclass="w-full mx-auto" imgclass="mx-auto"
              capclass="" src="/uploads/2023HFF-logo-horizontal-crop.png"
              width=800 link="" >}}
            sectionclass: bg-lessmoody bg-opacity-90 pb-12
          - _bookshop_name: media-text
            heading:
            text: >-
              <h2 style="color: white;">Holiday Family Fair</h2> <span
              style="color: white;">The Holiday Family Fair is a joyous part of
              Celebrate the Season's festivities. Families can join us, FREE of
              charge, at the Port of Anacortes Transit Shed for a day of live
              performances - featuring Island View Elementary, Mt Erie
              Elementary and Fidalgo Elementary School Choirs, free crafts, free
              cookies, the cherished Student Marketplace (students keep 100% of
              the money earned from the sale of their items!) and, of course,
              free photos with Santa. The next day, the Fidalgo Island Rotary
              Club takes over the Port and hosts the annual Gingerbread
              Contest.<br></span>

              <a href="/programs/holiday-fair" class="btn inline-block max-w-fit
              px-4 py-2" style="margin-top: 20px;">Learn More</a>
            image_right: true
            images:
              - title: Holiday Family Fair
                src: /uploads/2022-cts-04.jpg
              - title: Holiday Family Fair
                src: /uploads/2021-hff-2.jpg
              - title: Holiday Family Fair
                src: /uploads/2021-hff-8.jpg
              - title: Holiday Family Fair
                src: /uploads/2021-hff-10.jpg
              - title: Holiday Family Fair
                src: /uploads/2021-hff-11.jpg
            video: {}
            url:
            action:
            sectionclass: bg-lessmoody bg-opacity-90 2xl:px-24
            headingclass: font-aleo
            textclass:
            actionclass:
    sectionclass: >-
      bg-[url('/uploads/HFF2023-Top-bg-sm.png')] max-w-none !py-0 bg-fixed
      bg-cover background-repeat:no-repeat
  - _bookshop_name: color-bar
    class: bg-silver h-12
  - _bookshop_name: columns
    num_cols: 1
    columns:
      - name: Logo
        col_span: 1
        divclass: '!space-y-0'
        backgroundImage:
        column_components:
          - _bookshop_name: media-text
            heading:
            text: >
              Interested in hosting <br>

              a Holiday Party at the Port?


              Learn more:  


              <a href="/uploads/Sponsorship & Private Parties CTS 2023.pdf"
              download><span style="color: #9ed0f0">Download Flyer</span></a>
            image_right: false
            images:
              - title: Celebrate the Season
                src: /uploads/2021-hff-28.jpg
              - title: Celebrate the Season
                src: /uploads/2021-cts-12.jpg
              - title: Celebrate the Season
                src: /uploads/2021-cts-13.jpg
              - title: Celebrate the Season
                src: /uploads/2021-cts-16.jpg
            video: {}
            url:
            action:
            sectionclass: 2xl:px-24
            headingclass: font-aleo
            textclass: text-white text-xl font-caps font-bold !text-center
            actionclass:
    sectionclass: >-
      bg-[url('/uploads/private-party-bg-rr.jpg')] max-w-none !py-0 bg-fixed
      bg-cover
  - _bookshop_name: color-bar
    class: bg-ctsgreen h-12
---
