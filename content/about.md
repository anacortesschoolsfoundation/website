---
title: About Us
aliases:
  - /about-us/board-of-directors.html
  - /board
  - /about-us/volunteers.html
  - /about-us.html
  - /about-us/asf-history.html
bannerImage: /uploads/page-title-run-lower-r.jpg
description: >-
  Our board and staff are highly committed to ensuring ASF's resources best
  serve our students and our mission, to empower Anacortes students through
  scholarships and enriched learning opportunities.
keywords: >-
  anacortes schools foundation, donation, Anacortes School District, board of
  directors, board committees, Guidestar
featuredImage: /uploads/run-2022.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: About Us
    bannerImage: /uploads/page-title-run-lower-r.jpg
  - _bookshop_name: heading
    title: About Anacortes Schools Foundation
    subtitle:
    text:
    sectionclass: items-start
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: About
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >-
              ASF is led by a board of 16 directors and 1 part-time staff. Our
              board and staff are highly committed to ensuring ASF's resources
              best serve our students.


              ## Our Mission


              > Empower and inspire Anacortes students through scholarships and
              enriched learning opportunities.


              To accomplish this mission, ASF raises funds through two major
              annual events; the "[Fueling Education Fun Run](/programs/run)"
              and the "[Celebrate the Season Dinner &
              Auction](/programs/celebrate-the-season)."


              In addition, we solicit funds for the [Ready to Learn
              Fair](/programs/readytolearn), through our annual campaign and for
              specific programs through [ASF Designated Donations.](/giving)


              {{< asf-figure title="Anacortes School District"
              src="/uploads/asd_2color_horizontal.jpg" width=355
              link="http://www.asd103.org/" divclass="w-full md:w-auto
              md:float-right md:ml-8" figclass="mx-auto md:mx-0" >}}


              Anacortes Schools Foundation is proud to partner with [Anacortes
              School District](http://www.asd103.org/).


              ## Our Vision


              ASF builds relationships with donors who want to make a difference
              in our schools. Through funding and program support, we focus on
              helping the whole student thrive and achieve their highest
              potential.


              {{< asf-figure title="ASF Board Members"
              src="/uploads/run-2022.jpg" width=690 caption="ASF Board Members
              at the 2022 Fun Run" figclass="items-center" imgclass="border p-4
              shadow mb-2" >}}
      - name: Executives
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: directors
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: Board Committees
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title: Board Committees
            content: >-
              In addition to the seven committees listed below, new committees
              are established each year in support of the "[Celebrate the
              Season](/programs/celebrate-the-season)" and "[Fueling Education
              Run](/programs/run)." As needed, the Board may also appoint Ad Hoc
              committees for specific projects.


              * Fundraising and Marketing Committee

              * Scholarship Committee

              * Policies and Processes Committee

              * Human Resources

              * Finance and Investment Committee

              * Grant Committee

              * Compliance Committee


              The Board of Directors is a dedicated, working volunteer board
              comprised of up to 21 community and parent leaders, plus the
              Anacortes School District Superintendent who acts as Ex-Officio
              and liaison with the school district.


              If you have additional questions or for more information, please
              complete the Contact Form on our "Contact Us" page.&nbsp;
      - name: Committee Meeting Image
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title:
            content: >
              {{< asf-figure title="The Scholarship Committee zooming in for a
              meeting." caption="The Scholarship Committee zooming in for a
              meeting." capclass="mt-2 text-asfpurple" figclass="items-center"
              imgclass="border p-4 shadow"src="/uploads/about-1.jpg" width=392
              >}}
  - _bookshop_name: content
    title: Executive Director
    content: >-
      We are currently in the process of hiring a new Executive Director. Stay
      tuned for more information!&nbsp;
  - _bookshop_name: content
    title: Guidestar
    content: >
      {{< asf-figure title="Guidestar Logo" divclass="float-left mr-8 mb-6"
      figclass="items-start" imgclass="border p-4 shadow"
      link="https://www.guidestar.org/profile/91-1263495"
      src="https://widgets.guidestar.org/gximage2?o=8316301&l=v4">}}


      Anacortes School Foundation has earned a 2020 Gold Seal by adding
      information to our Nonprofit Profile on
      [GuideStar](https://www.guidestar.org/profile/91-1263495). Now, our
      community members as well as 10 million + GuideStar users can find
      in-depth information about ASF's goals, strategies, capabilities and
      progress. We're shining a spotlight on the difference we help make in the
      world.
  - _bookshop_name: content
    title: National Association of Education Foundations
    content: >
      {{< asf-figure title="National Association of Education Foundations"
      divclass="float-left mr-8 mb-6" figclass="items-start" imgclass="border
      p-4 shadow" width=250 src="/uploads/NEA-logo.png">}}


      Anacortes School Foundation is a member of the National Association of
      Education Foundations.
  - _bookshop_name: content
    title: Form 990
    content: >-
      [2022](/uploads/2022-Form-990.pdf) |
      [2021](/uploads/13a-2021%20Form%20990%20-%20ASF%20%20FINAL%20Sch%20B%20redacted.pdf) |
      [2020](/uploads/2020-Form-990-Anacortes-Schools-Foundation.pdf) |
      [2019](/uploads/2019-Form-990-ASF.pdf)
  - _bookshop_name: content
    title: Annual Report
    content: >-
      [2022-2023](/uploads/ASF%202023%20Annual%20Report-FINAL.pdf) |
      [2021-2022](/uploads/ASF%202022%20Annual%20Report%20v6%20-%20FINAL.pdf) |
      [2020-2021](/uploads/2020-ASF-Annual-Report-FINAL.pdf)
---
