---
title: "What do we fund?"
subtitle: "The Anacortes Schools Foundation (ASF) works with the Anacortes School District, to help all students achieve the promise of their potential. We believe that education establishes a foundation for opportunity, growth and success in life."
author: 
date: 2023-05-14
description: "The Anacortes Schools Foundation (ASF) works with the Anacortes School District, to help all students achieve the promise of their potential. We believe that education establishes a foundation for opportunity, growth and success in life."
keywords: "STEM, field trips, what does ASF pay for, Anacortes scholarships"
featuredImage: /uploads/blog-WDWF.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- stem
- scholarships
- ready-to-learn-fair
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              We raise private support so that our public schools have the ability to provide the highest quality education for ALL our students. We know that strong, healthy schools contribute to a strong and healthy community, and we believe that every child in Anacortes has a right to the best education available anywhere.

              ## We fund DISCOVERY

              ### STEM support for elementary, middle and high school students

              {{< asf-figure title="ASF Discovery Funding" divclass="w-full md:w-96 md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="w-full md:w-96" capclass="" src="/uploads/blog-fund-disc.jpg" width=400 link="" >}}

              For the past decade, ASF has worked with the district to match STEM curriculum with grade-wide enrichment and hands-on experiences. Some of the STEM programs we support:

              Fidalgo, Island View and Mount Erie Elementary Schools:

              * Field trips to Padilla Bay, Island Adventures Salish Sea Science Trip, Spark Museum and Mountain School
              * Science classroom equipment including digital microscopes, 3-D printers, Snap Circuits, Invent Kits and more

              Anacortes Middle School:

              * NEW 7th Grade Salish Sea Discovery Trip
              * Garden to Kitchen support including a new greenhouse
              * Classroom equipment including microscopes and a FitLight system

              Anacortes High School:

              * Support for Green Club projects
              * Over 40 STEM specific scholarships offered annually for students planning to study engineering, math, environment and the sciences

              ## We fund DREAMS

              ### ASF has been awarding scholarships since 1984

              {{< asf-figure title="ASF Dreams Funding" divclass="w-full md:w-96 md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="w-full md:w-96" capclass="" src="/uploads/blog-fund-dreams291.jpg" width=400 link="" >}}

              With just one application, students have access to 145 scholarships totaling more than $350,000 in awards. Scholarships are open to students pursuing 4-year and 2-year degrees or vocational pathways.

              ## We fund HOPE

              ### Schools can play an important role in helping children and youth get access to mental health

              When struggling with depression, anxiety or other mental health issues, working on assignments and attending classes can become next to impossible. That is why ASF is committed to funding social workers in our schools and providing access to programs like RULER which help students kindergarten through 5th grade learn to use their emotions effectively, improving everything from decision-making and judgment to physical and mental health.

              ## We fund PRIDE

              ### Backpacks, supplies, books, clothing and so much more

              {{< asf-figure title="ASF Hope Funding" divclass="w-full md:w-96 md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="w-full md:w-96" capclass="" src="/uploads/blog-fund-pride.png" width=400 link="" >}}

              Our annual Ready to Learn Fair helps students start the school year off on the right foot. ASF provides school essentials including backpacks, supplies, haircuts, socks and  underwear and critical resources to kids who need a little extra help. Our [23rd annual Ready to Learn Fair](http://localhost:1313/programs/readytolearn/) is scheduled for Saturday, August 19, 2023.

              ## We fund LEARNING
              ### Preschool Scholarships, Impact and School Block Grants

              ASF impacts student learning through a variety programs including:

              * Preschool Scholarships - preparing kids socially and academically for a successful integration into kindergarten
              * Impact K-5 Summer Learning - 4 weeks of targeted support in math, reading and science, plus movement, music and more
              * School Block Grants - funds to address the highest needs in each school

              ## We fund CREATIVITY

              ### Visual and Performing Arts have a significant positive impact on academic and social development

              {{< asf-figure title="ASF Hope Funding" divclass="w-full md:w-96 md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="w-full md:w-96" capclass="" src="/uploads/blog-fund-creativity.png" width=400 link="" >}}

              ASF supports the visual and performing arts through:

              Resident Artists in Schools - a partnership between ASF, the Anacortes Arts Festival and each elementary school's PTA to fund monthly arts classes for all grades 
              Local Artists in Schools, Poetry Outloud and Field trips to MoNA
              Funding at the middle school and high school level for drama, choir, band, ceramics, drawing, painting, photography and digital media

              Please consider joining us in funding discovery, dreams, hope, creativity, learning and pride to help ASF give Anacortes students the strongest foundation possible for their future! [Donate today!](/giving)
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
