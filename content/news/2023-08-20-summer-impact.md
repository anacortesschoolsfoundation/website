---
title: "Summer Impact a Success"
subtitle: "Summer Impact was created to engage students in academics, with a focus on foundational reading and math skills along with social-emotional learning, while also offering fun enrichment activities."
author: 
date: 2023-08-20
description: "Summer Impact was created to engage students in academics, with a focus on foundational reading and math skills along with social-emotional learning, while also offering fun enrichment activities."
keywords: "summer school, ipmact school, summer learning"
featuredImage: /uploads/prog-impact.jpg
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Summer Impact
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Summertime means no school, swim lessons, special events at our public library, trips to the beach and so much more. But for many kids whose parents work full or even part-time, it can mean no one at home to read to them or to take them to the science day at the library. For some, it can even mean summer slide, or learning loss. Most students make reading and math gains throughout the skill year, but if they don't read, or are not read to, during the summer, they can actually lose those skills and start the next fall behind where they left in the spring. 

              The idea was to make the four weeks of Impact full of learning but also full of fun. But it also had to be free. Transportation and food needed to be provided. And every year, thanks to generous donors, it has been.

              This year, 109 K-5th graders attended five days a week, three hours a day for four weeks with 27 staff members (12 teachers, 12 paraeducators, one nurse, one administrator and one administrative assistant) working to make the program a success. Average attendance was 83%. Three buses transported the 52 students who opted to ride, and 100 lunches were served each day.

              A typical day included English Language Arts, math, social-emotional learning and enrichment electives (movement and dance, art, music, acting). Students learned social studies (Since Time Immemorial) and science through weekly field trips and outdoor learning opportunities. We're excited to report that this year was a smashing success with students showing growth in all three assessed areas (math, reading and social-emotional learning). Significant gains were made in reading by 3rd-5th graders (increased words correct per minute) and K-2nd graders increased their ability to correctly identify letter names and sounds then transfer that knowledge to blending and effectively reading words they weren't able to read at the start of the program. And in math, the majority of students showed mastery of concepts taught by scoring several points higher on their post assessments. 

              Don't take it from us. Here's what students, parents and teachers had to say:

              > I have seen so much improvement in my daughter's math this summer. I feel like she is ready to start 1st grade next year.

              > It is so great to see how much progress the students made from pre-assessment to post assessment.

              > I really like coming to school in the summer.

              > Students learned a traditional Samish gesture and to speak a phrase [Hóy7sxwq'e](https://www.samishtribe.nsn.us/departments/language/Samish-Word-Library/greetings/thank-you) for 'thank you' on their first field trip. At a later field trip, we were lucky enough to see a group of Samish People on the beach as they prepared for their canoe journey. After speaking with them, many of our students remembered and used the phrase and gesture.

              This crucial program would not be possible without community partnerships and incredibly generous donations. This year's field trip partners included Washington State Parks (Ranger Joy Kacoroski), Friends of the Forest (Hannah Katz) and Samish Tribe (Denise Crowe). In addition, the enrichment/elective partners, coordinated by Melissa Turnage of Fidalgo Danceworks, included Fidalgo Danceworks (Janet Wilken, Alexandria Schafer), ACT (Lucy Price), Anacortes Arts Festival (Meredith Mcllmoyle) and Anacortes Music Project (Pearl Tottenham).

              {{< video youtubeID="EmMacC12IcU" >}}
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
