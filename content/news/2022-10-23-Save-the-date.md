---
title: "Save the Date to Celebrate with ASF!"
subtitle: "ASF invites you to save the date for the 10th Annual Celebrate the Season Gala Fundraiser Dinner! The theme this year is 'A Storybook Holiday' and the evening will be filled with celebration, a gourmet dinner, exciting live and silent auction items, entertainment, and so much more."
author: 
date: 2022-10-23
description: "ASF invites you to save the date for the 10th Annual Celebrate the Season Gala Fundraiser Dinner! The theme this year is 'A Storybook Holiday' and the evening will be filled with celebration, a gourmet dinner, exciting live and silent auction items, entertainment, and so much more."
keywords: "Anacortes schools foundation, celebrate, fundraiser, school fundraiser, christmas celebration fundraiser, holiday fair anacortes, anacortes, holiday, fair, celebrate the season, gala"
featuredImage: /uploads/CTS2022-Blog-r.jpg
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Celebrate the Season
featured: true
aliases:
    - /news/2022-10-23_Save-the-date
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Tickets go on sale in early November and the first 50 households to register will receive an event goodie bag delivered to their door. We hope you will  join us on December 3 as ASF writes the next chapter of support for Anacortes students!

              Proceeds of the event benefit Anacortes students and ASF enrichment programs including STEM, literacy, the arts, mental health, scholarships and summer learning. If you are not able to attend the gala but would like to help our students, you can donate through [Anacortes Schools Foundation | Make a Donation](/giving).
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---

