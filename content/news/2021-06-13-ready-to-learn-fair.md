---
title: The Annual Ready to Learn Fair is Back!
subtitle: ASF is thrilled to announce that the Ready to Learn Fair will be back again this August with a full set of FREE services!
author: 
date: 2021-06-13
description: The Ready to Learn Fair will be held on Saturday, August 21st at Anacortes High School from 9am to 12pm. Save the date and keep an eye out on our website and social media for more information regarding online registration. 
keywords: ready to learn fair, school supplies, anacortes school supplies fair, anacortes ready to learn fair, anacortes school district
featuredImage: /uploads/RTLF-yard-sign.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Ready to Learn Fair
featured: 
aliases:
    - /news/2021-ready-to-learn-fair
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Since 2000, the Ready to Learn Fair has been providing hundreds of Anacortes students with free school supplies to start their school year off right and ASF is thrilled to announce that the Ready to Learn Fair will be back again this August with a full set of FREE services! Last year, it was necessary to transition the Fair to a drive-through, backpack and supplies only event. The usual fair opportunities such as free books, free clothing and community partner booths were dearly missed and we couldn't be happier to be offering these options again for Anacortes families.

              If you have any questions, please reach out to the Ready to Learn Fair Committee Chair, Treva, at tking@asd103.org.   

              The Ready to Learn Fair would not be possible without the support of individuals and business sponsors (and MANY amazing volunteers). If you are interested in volunteering at this year's event, please complete our Volunteer Interest Form. Thank you!
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
