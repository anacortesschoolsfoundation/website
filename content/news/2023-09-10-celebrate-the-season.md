---
title: "Plans underway for a nautical-themed Celebrate the 'Sea'son!"
subtitle: "Event Chair Carrie Worra and members of the Anacortes Schools Foundation Celebrate the Season Committee gathered this week to begin planning for this year's much anticipated event."
author: 
date: 2023-09-10
description: "Event Chair Carrie Worra and members of the Anacortes Schools Foundation Celebrate the Season Committee gathered this week to begin planning for this year's much anticipated event."
keywords: "celebrate the season, anacortes schools fundraiser"
featuredImage: /uploads/SEA-son.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Celebrate the Season
top: true
featured: true
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Event Chair Carrie Worra and members of the Anacortes Schools Foundation Celebrate the Season Committee gathered this week to begin planning for this year's much anticipated event. The gala fundraiser, now in its 11th year, will take place on Saturday, December 2 and will feature delicious food, signature cocktails, wine and beer, silent and live auctions, entertainment and more. An elegant nautical theme honoring the event's final year in the Transit Shed's dockside setting is planned. "Every year this amazing committee outdoes itself" said ASF Executive Director Marta McClintock. "The creativity and dedication of this team are unparalleled." Celebrate the Season is the main fundraising event for the Anacortes Schools Foundation. Proceeds support enrichment and programs for Anacortes students Kindergarten through 12th grade.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
