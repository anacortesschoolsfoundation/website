---
title: Ready to Learn Fair Back In Person!
subtitle: ASF is excited to announce the full, in-person return of the Ready to Learn Fair on Saturday, August 21 from 9am-12pm at Anacortes High School.
author: 
date: 2021-08-18
description: ASF is excited to announce the full, in-person return of the Ready to Learn Fair on Saturday, August 21 from 9am-12pm at Anacortes High School. We know going back to school this fall will be an especially important milestone and we also know that many families are suffering from the economic impact of the pandemic. That is why we are more committed than ever to providing students with back to school essentials and ensuring they start the year off with pride!
keywords: ready to learn fair, school supplies, anacortes school supplies fair, anacortes ready to learn fair, anacortes school district
featuredImage: /uploads/2021-rtl-svcs.jpg
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Ready to Learn Fair
featured: true
aliases:
    - /news/2021-rtl-is-back
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              **FREE Backpacks, books, supplies, clothing and more!**

              Saturday, August 21  
              9am-12pm  
              **[Anacortes High School](https://goo.gl/maps/gXWdm7jTMeVRLn3F6)**  

              We know going back to school this fall will be an especially important milestone and we also know that many families are suffering from the economic impact of the pandemic. That is why we are more committed than ever to providing students with back to school essentials and ensuring they start the year off with pride!

              ### Backpacks, Books, and Clothing

              In true Ready to Learn Fair fashion, not only will we be offering FREE backpacks full of grade appropriate school supplies for K-12 students, but families can stop over at the clothing area for new socks and/or underwear in addition to their pick of FREE gently used (and freshly washed - Thank you Soroptimist!) outerwear. 

              Afterwards, don't forget the AHS library will be packed with thousands of new FREE books for all reading levels for students to choose from! We are especially grateful to Watermark Book Co. for donating over 200 new books for this year's Fair!! Volunteers will be on hand to help students find a variety of books that interest them. 

              ### Zero Waste

              {{< asf-figure title="Zero Waste Event" divclass="w-full md:w-96 md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="w-full md:w-96" capclass="" src="/uploads/2021-rtlf-zero.png" width=300 link="" >}}

              We are proud to be a Zero Waste event, so we appreciate fairgoers remembering to bring their own cloth or recycled bag (or try out their student's new backpack!) to carry their new clothing and books home in. Water bottle filling stations are also available throughout the building. 

              ### But wait... there's more!

              In addition, over a dozen community partners will be filling the halls of AHS to provide families with valuable information on local resources. ASF will also be providing an updated Community Resource Information Sheet to all families attending the Fair. Knowing where to find the information you're looking for is half the battle! 

              The Anacortes Lions Club will be offering students free vision screenings and Skagit County Health will have a pop-up vaccine clinic outside the Main Entrance to provide all 3 shots to all who are interested. 

              The Ready to Learn Fair wouldn't be complete without the Kiwanis Sunrisers cooking up delicious hot dog meals! They are back again providing a FREE hot dog meal to all students 16 and under. All others are welcome to support the Sunrisers by purchasing a hot dog meal of their own. 

              This event is made possible through the tireless efforts of over 50 volunteers and thanks to the generous partnership of many individuals and organizations. Special thanks to this year's presenting sponsors: Anacortes Rotary Club, Marathon Petroleum and Windermere Property Management as well as major supporters: Fidalgo Island Rotary, Kiwanis Noon Club, Heritage Bank and Watermark Book Co.

              {{< asf-figure title="Ready to Learn Fair Map" divclass="w-full" figclass="mx-auto" imgclass="w-full" capclass="" src="/uploads/rtl-map.jpg" width=800 link="" >}}

              {{< asf-figure title="Ready to Learn Fair Map - p.1" divclass="w-full mb-8" figclass="mx-auto border" imgclass="w-full" capclass="" src="/uploads/2021_RTLF-pg1.jpg" width=800 link="" >}}

              {{< asf-figure title="Ready to Learn Fair Map - p.2" divclass="w-full" figclass="mx-auto border" imgclass="w-full" capclass="" src="/uploads/2021_RTLF-pg2.jpg" width=800 link="" >}}

              The Anacortes Ready to Learn Fair has been providing support to Anacortes students since 2000. If you'd like to support to the Ready to Learn Fair to ensure we are able to continue supporting Anacortes students for years to come, please **[donate](/giving)** today.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---

 