---
title: Anacortes Students "Seas the Day"
subtitle: 
author: Treva King
date: 2021-02-01
description: Island Adventures has teamed up with ASF to send all our 4th graders on this hands-on STEM experience. 
keywords: Island Adventures, STEM trips, anacortes boat
featuredImage: /uploads/island-adventures-news.jpg
photoPosition: center
featuredPhotoCredit: Lisa Kuhnlein
featuredPhotoWebsite: https://www.kuhnlein.com/
categories:
- STEM
aliases:
    - /news/anacortes-students-seas-the-day
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Since 1996, **[Island Adventures](https://www.island-adventures.com/)** has been taking Anacortes elementary students out on their vessel to explore the living world of the water. 

              Starting with Carol Strandberg's second grade class, the annual field trip has evolved from a pleasurable boat ride to a virtual floating classroom, with on-board naturalists teaching students the elements of oceanography, including plankton tow, turbidity, and pH scope. The trip grew from one Mt. Erie classroom to all second grade classes at the school, eventually encompassing all second grade classes in the district. Now, the Salish Sea Science Trip is aligned with the fourth grade curriculum, allowing teachers to better prep the students beforehand. 

              Owner Shane Aggergaard moved to Anacortes at the age of three when his parents handpicked the town based on the schools and the sense of community. Island Adventures has teamed up with ASF to send all our 4th graders on this hands-on STEM experience. He and wife Jen say they love the partnership with ASF because it keeps the focus on the kids. 

              > "It's important to us to foster learning and give students an opportunity to soak up everything they can here in their amazing backyard."
              >
              >-- Jen Aggergaard, Island Adventures
              {.not-prose}

              "It would be a shame to live on this island and not get the kids out on the water," says Jen. Shane adds that the most rewarding part of the job is watching the kids' sense of awe when they learn about all that lives below the surface. 

              Thank you, Shane and Jen Aggergaard, for giving back to your community in a meaningful way by sharing your love of the water and of learning with our students.

              {{< asf-figure title="ASF Discovery Funding" divclass="w-full" figclass="mx-auto" imgclass="w-full" capclass="" src="/uploads/prog-stem4.jpg" width=800 link="" >}}
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
