---
title: "ASF Scholarships - Celebrating 40 Years of Giving"
subtitle: "In 1982, Arnold and Cressa Houle made a gift with the intent to support an AHS graduate planning to attend a 2 or 4-year college or vocational school."
author: 
date: 2022-03-15
description: "In 1982, Arnold and Cressa Houle made a gift with the intent to support an AHS graduate planning to attend a 2 or 4-year college or vocational school."
keywords: "Arnold Houle, Cressa Houle, Ryan Walters, ASF scholarships, Anacortes scholarships, Anacortes Schools, Anacortes School District, anacortes port, celebrate anacortes, anacortes school district, anacortes stem grants, anacortes scholarship fund fundraiser"
featuredImage: /uploads/40-yrs-houle-white.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Scholarships
featured: true
aliases:
    - /news/2022-celebrating-40-years
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Arnold and Cressa did not have any children but they believed strongly in education and established the [Arnold and Cressa Houle Scholarship](https://www.anacortesschoolsfoundation.org/scholarships/arnold-and-cresida-houle-memorial/) to honor a student who showed, above all, responsible citizenship. In 1983, the first Houle Scholarships were awarded to Mollie Barcott and Richard Harris and later to Anacortes City Councilmember Ryan Walters. The 2021 recipient, Ava Kephart is an accomplished athlete and scholar, was a  volunteer soccer coach and Link Crew Leader and is currently attending St. Edward's University, where she is studying computer science. Arnold and Cressa recognized early on that college costs could be a significant burden for many students and their families. They also believed that students who were supported by their community would learn the importance of philanthropy and giving back. Forty years later, and from the seed of the Houle's gift, the Anacortes Schools Foundation has grown to encompass 138 scholarships and plans to award nearly $350,000 on June 6, 2022 at the ASF Scholarship Awards Ceremony at AHS.

              [ASF scholarships](https://www.anacortesschoolsfoundation.org/scholarships/) come in a variety of shapes and sizes and from a variety of sources including estates and businesses, ASF fundraising events and support from individual donors. Students can be considered for most ASF scholarships, including several new endowed scholarships by completing on general application.

              New 2022 scholarships include the [Dorothy Ann Lione Nursing Scholarship](https://www.anacortesschoolsfoundation.org/scholarships/dorothy-ann-lione-nursing/), established by Mark Lione, owner of Cap Sante Inn and longtime advocate of education, the [Lynn Pletcher Memorial Scholarship](https://www.anacortesschoolsfoundation.org/scholarships/lynn-pletcher-memorial/), established by new Anacortes residents Brett and Jennifer Pletcher honoring Brett's fathers' 40-year career in public education, and the [Curtis Family Scholarship for Children of First Responders](https://www.anacortesschoolsfoundation.org/scholarships/curtis-family/) established by Jack and Josette Curtis which recognizes the service and sacrifices made by families of first responders. In addition to scholarships awarded by community organizations including the [Anacortes Noon Kiwanis Club](https://www.anacortesschoolsfoundation.org/scholarships/kiwanis-anacortes-noon-club-foundation/), [Anacortes Sunrisers](https://www.anacortesschoolsfoundation.org/scholarships/kiwanis-sunrisers/), [Friends of the ACFL](https://www.anacortesschoolsfoundation.org/scholarships/friends-of-the-forest/), [AHS PTSA](https://www.anacortesschoolsfoundation.org/scholarships/anacortes-high-school-ptsa/), and [Anacortes Sister Cities Organization](https://www.anacortesschoolsfoundation.org/scholarships/anacortes-sister-city-association/), this year, ASF is also honored to help facilitate awards made by the [American Association of University Women](https://www.anacortesschoolsfoundation.org/search/?query=AAUW&search=). The Anacortes Schools Foundation is honored to have provided support to over 1000 students through scholarships over the last 40 years and looks forward to many more years of making students' dreams come true.

              Information about all ASF's current scholarships can be found [here](https://www.anacortesschoolsfoundation.org/scholarships/#all). For more information about how you can support Anacortes students by helping them further their education at college or through technical and trade programs, please visit our [Scholarship Support page](https://www.anacortesschoolsfoundation.org/giving/scholarship/).
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---


