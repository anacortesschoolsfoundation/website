---
title: Legacy Society Established in 2018
subtitle:
author: Marta McClintock
date: 2021-01-01T00:00:00Z
description: >-
  ASF Legacy Society provides an opportunity to impact students for years to
  come. In early 2018, ASF officially launched a planned giving program and
  established a Legacy Society to recognize donors who have included ASF in
  their will.
keywords: legacy society, will donation, scholarship in will
featuredImage: /uploads/donor-monaghan.jpg
photoPosition: center
featuredPhotoCredit:
featuredPhotoWebsite:
categories:
  - Scholarships
aliases:
  - /news/legacy-society
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News Content
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            content: >-
              The first donors to join the ASF Legacy Society were Anacortes
              residents Greg and Sue Monaghan. The Monaghans believe "few causes
              in life can have a greater influence on a young person's journey
              than the gift of a formal education or advanced training in a
              trade." They have pledged to endow a scholarship for a student
              graduating from Cap Sante High School through their estate. And,
              because they want to see their giving in action and meet some of
              their recipients, they are also funding an annual scholarship.


              To invest in a better future for all Anacortes students or to get
              involved, please contact Marta McClintock at
              **[marta@asfkids.org](mailto:marta@asfkids.org)** or 360-488-2343
              or visit the **[Giving](/giving)** page.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: news-sidebar
---
