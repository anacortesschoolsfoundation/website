---
title: Celebrate the Season - Raising funds to support student mental health and other ASF programs
subtitle: "The holidays are upon us and that means it's time to \"Celebrate the Season\" and support Anacortes students!"
author: 
date: 2021-10-03
description: "The holidays are upon us and that means it's time to \"Celebrate the Season\" and support Anacortes students!"
keywords: "Anacortes Schools, Anacortes School District, Celebrate the season, anacortes port, anacortes school
fundraiser, anacortes schools foundation, anacortes school foundation fundraiser, anacortes auction, anacortes school
auction, art contest, anacortes art contest, anacortes school art contest, anacortes foundation dinner, anacortes school
celebration, school foundation fundraiser anacortes, anacortes district fundraiser, celebrate anacortes, anacortes
school district, anacortes stem grants, anacortes scholarship fund fundraiser, anacortes celebration, ASD fundraiser,
ASF Fundraiser"
featuredImage: /uploads/image.png
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Celebrate the Season
featured: true
aliases:
    - /news/2021-cts-mental-health
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              "We're thrilled that the Celebrate the Season Gala Fundraiser Dinner is back in-person," said ASF Executive Director Marta McClintock about the December 4th event which will take place at the Port of Anacortes Transit Shed. "We are also excited to offer a hybrid or virtual option for those who wish to support, but feel more comfortable staying home" she added. In addition to all the "Celebrate" festivities, the first 100 guests to register for either event will receive a free "Celebrate the Season" Event Bag delivered to their home on November 18th. Find out more about both options [HERE.](https://www.anacortesschoolsfoundation.org/programs/celebrate2021/)

              "Celebrate the Season" isn't just a party; it's an opportunity for the community to come together and help fund ASF's top priority this year: mental health supports for all Anacortes students" said McClintock. "We know our students and families have been through a lot and we also know that when we invest in children's mental health, we improve the lives of our entire community" added McClintock.

              According to ASD Superintendent Justin Irish, this focus on funding for mental health could not come at a more critical time. Irish said that while students returning to full-time in person school this fall was something to celebrate, he knows that the pandemic and related school disruptions continue to have a profound impact on the mental and emotional well-being of Anacortes students. "We know mental health disorders can affect classroom learning and social interactions, both of which are critical to students' success," said Irish. "Since children spend much of their productive time in educational settings, schools offer a unique opportunity for early identification, prevention, and interventions that serve students where they already are. Teaming up with ASF and Island Hospital to provide appropriate supports Kindergarten through 12th grade is the most amazing gift for all our students," said Irish.
                
              Many of our students are dealing with trauma as a result of the pandemic, but even before COVID-19, the data clearly indicated that there was a growing mental health epidemic among K-12 students. Academic stressors, social isolation, family and economic stressors, and anxiety over COVID all play a role in the trauma students have faced. But, with ASF's help, ASD plans to strengthen and expand mental health supports, providing everything from social emotional programs that focus on prevention, wellness, and building coping skills, to help for students who have chronic or intense mental-health needs that require medication, daily counseling or check-ins, or close coordination with a clinical professional such as a psychiatrist. ASF plans to provide grants for a social worker to support K-5th grade at all three elementary schools, a partial grant for a social worker at AMS, a grant for an On Time Graduation Success Support in the AHS Counseling Office and continued funding for Social Emotional Programs including RULER, Where Everyone Belongs (WEB), Character Strong and Signs of Suicide.
              
              School-based and school-linked mental health services reduce barriers to youth and families getting needed treatment and supports. That is why ASF has set an ambitious goal of raising money to support three years of funding for the above priorities which support ALL ASD students. 
              
              "Celebrate the Season" would not come together without the important support of many. ASF is grateful for Gala Co-Chairs Kim Davis and Carrie Worra, Holiday Fair and Event Chair Megan Atterberry and their very dedicated committee. We are also incredibly appreciative of our generous donors and many sponsors, including Marathon Petroleum who each year supports the event's Raise the Paddle with $25,000 in matching funds. 

              For more information about "Celebrate the Season" or how to support mental health in Anacortes schools, please [click HERE.](https://www.anacortesschoolsfoundation.org/programs/celebrate2021/)
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
