---
title: Running to Fuel Education
subtitle: Since 1958, Puget Sound Refinery employees have watched their kids learn
  and grow in Anacortes schools until eventually donning that purple cap and gown.
author: Andrea Petrich
date: 2021-08-23
description: Since 1958, Puget Sound Refinery employees have watched their kids learn
  and grow in Anacortes schools until eventually donning that purple cap and gown.
keywords: Holly Frontier, Shell PSR, 5k, Anacortes Fun Run, 5k fundraiser, Shell, Fueling Education, HF Sinclair, Fueling Education Fun Run
featuredImage: "/uploads/2021-5k-Logo-h.jpg"
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- Run
top: false
featured: false
aliases:
    - /news/2021-running-to-fuel-education
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              Squeals of excitement from backyard sprinkler parties, the light sizzle of home-grown zucchini on the barbeque and the repetitive sound of your sneaker sole hitting the warm pavement of the Tommy Thompson Trail. Those are the sounds of the perfect days of summer here in Anacortes, at least for those preparing for Puget Sound Refinery's Fueling Education Fun Run.

              ### Generations of students  
              Since 1958, Puget Sound Refinery employees have watched their kids learn and grow in Anacortes schools until eventually donning that purple cap and gown. Today, many PSR employees are proud graduates of Anacortes High School, some of those are the second, third or even fourth generation of families to go through Anacortes schools and work at the refinery.
              "The refinery and its support for this community is why we ended up in Anacortes and I couldn't have chosen a better school system for myself or my children," said father of AHS class of 2014, 2015 and 2021 graduates, proud AHS alumni and Puget Sound Refinery Production Excellence Manager Jason Smolsnik. "The teachers and coaches were the difference for our sons. We can't thank them or our community enough." 

              ### The perfect fit  
              As PSR looked for ways to support our community through an annual event, the beneficiary seemed obvious.  Organizers decided to help fund the organization that educates many of its employee's children, supports them all equitably and is where our future workforce stems.

              So, in fall of 2015, the Fueling Education Fun Run was born to raise money for the Anacortes Schools Foundation. 

              > "We wanted to leave a big impression in our community and thought a great way to do it was by pounding the pavement to participate in a healthy activity while enjoying the beauty of our area," said Race Director and Puget Sound Refinery Technology & Engineering Manager James Steller.
              {.not-prose}

              The Anacortes School District educates around 2,500 children each year, graduates 89 percent of high schoolers in four years and employs nearly 400 people in our community, 200 of those are educators. ASF supports those teacher's and support staff's efforts and provides additional opportunities to students through programs and activities that fall outside of the district's general operating expenses. The refinery's Fueling Education Fun Run generates significant funding each year to help support the foundation's mission.

              Since 2015, the Fun Run has awarded more than $60,000 in scholarships to Anacortes High School graduates and raised more than $250,000 to help fuel education in our community.

              >"This annual partnership allows us to support more students, more programs and have a great time in the process," said Anacortes Schools Foundation Executive Director Marta McClintock.
              {.not-prose}

              Seven years later, we're still running.

              ### Fun Run growth  
              Things look a little different now than they did when we first began. Participation has increased, race-day activities have been added and a virtual year has been completed (thanks, COVID) but the goal remains the same. Raising tens of thousands of dollars each fall to support Anacortes students through scholarships, a direct grant to the ASD school with the highest student participation rate in the Fun Run and with changing annual needs identified by ASD – most recently for Science-To-Go kits and COVID-related technology updates.

              ### Get ready to run  
              So, as the sun sets on summer barbeques and pool parties and back-to-school and fall harvest season moves in – keep those running shoes accessible. The committee hopes you and your family will join the PSR and ASF teams for the 7th Annual Fueling Education Fun Run on Sunday, Sept. 19. 

              Whether you're a runner, a walker or only move quickly when you're avoiding the backyard sprinkler, your registration will continue to help us Fuel Education in Anacortes as we work with Anacortes Schools Foundation to foster excellence in education, develop our future workforce and continue to nurture all the children in our community.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
