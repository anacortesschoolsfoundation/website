---
title: "ASF Announces NEW School Block Grants"
subtitle: "On September 25, the Anacortes Schools Foundation presented ASD principals with a BIG check in the amount of $50,080. Funding is based on enrollment numbers and will be disbursed to each school at a rate of $20 per student. "
author: 
date: 2022-10-02
description: "On September 25, the Anacortes Schools Foundation presented ASD principals with a BIG check in the amount of $50,080. Funding is based on enrollment numbers and will be disbursed to each school at a rate of $20 per student. "
keywords: "Anacortes schools foundation, grants, scholarship, block grant, block grants"
featuredImage: /uploads/asf-check.jpg
photoPosition: center
featuredPhotoCredit: 
featuredPhotoWebsite: 
categories:
- scholarships
featured: true
aliases:
    - /news/2022-10-01_ASF-Announces-NEW-School-Block-Grants
layout: blocks
content_blocks:
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: News
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: content-news
            
            content: |
              The ASF School Block Grants are a new funding source that provides principals with the authority to use funds in accordance with ASF's mission, and the flexibility to make investments in initiatives that reflect the needs of their individual schools.

              Connecting and collaborating with the district and supporting all students is at the forefront of all the Anacortes Schools Foundation does. This is reflected in our mission statement:  Empower and inspire all Anacortes students through scholarships and enriched learning opportunities.

              Furthermore, our new vision statement - ASF builds relationships with donors who want to make a difference in our schools. Through funding and program support, we focus on helping the whole student thrive and achieve their highest potential - demonstrates the aspiration of the foundation to have all community stakeholders invest in ASF and support exciting new opportunities like these grants.

              >The School Block Grants are a great new source of funding for our schools," said ASF Executive Director Marta McClintock, adding, "this support will empower each school to make meaningful, strategic decisions about where funds are most needed.

              Funding for these grants is made possible through ASF 2022 Annual Campaign gifts and proceeds from 2021 events that were not already earmarked or designated.
      - name: Sidebar
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: news-sidebar
---
