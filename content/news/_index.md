---
title: "News"
aliases:
    - /news/161-anacortes-middle-school-students-win-in-seattle.html
featuredImage: /uploads/images/logo@2x.png
description: Anacortes Schools Foundation - all current news
keywords: Anacortes Schools, Anacortes High School, Anacortes students, scholarships, reunions, ready to learn fair, school supplies, anacortes foundation, school foundation, anacortes scholarships
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: News
    bannerImage: "/uploads/page-title-rtl-2.jpg"
  - _bookshop_name: news-paginated
---
