---
title: Homepage
description: >-
  Anacortes Schools Foundation supports Anacortes students and raises $145K
  annually for COVID Relief, STEM enrichment programs, Visual and Performing
  Arts programs, ASF's annual Ready to Learn Fair, Impact Summer School,
  Preschool Scholarships, and MORE!
keywords: >-
  Anacortes, School, Anacortes High School, Anacortes students, scholarships,
  reunions, ready to learn fair, school supplies, anacortes foundation, school
  foundation, anacortes scholarships
featuredImage: /uploads/news-rtl.jpg
layout: blocks
content_blocks:
  - _bookshop_name: slideshow
    slides:
      - group: Celebrate the Season
        heading: Celebrate the Season
        description: >-
          Every December, ASF hosts a series of events including the Gala
          Fundraiser Dinner and FREE Family Holiday Fair
        image: /uploads/committee-photo-800x534.jpg
        link_url: /programs/celebrate-the-season/
        link_text: Read more
        meta:
      - group: STEM
        heading: Cause for Celebration
        description: >-
          This year, we celebrate a decade of STEM support in Anacortes schools
          and the dreamers who made it a reality
        image: /uploads/News-STEMwomen.jpg
        link_url: /news/2023-05-07-stem-women/
        link_text: Read more
        meta:
      - group: Art Enrichment
        heading: Support for Visual and Performing Arts
        description: >-
          ASF awards grants to choir, band, drama, digital media, ceramics, the
          After School Arts Program and more
        image: /uploads/news-arts.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: STEM
        heading: STEM Programs K-12
        description: >-
          ASF has a long history of providing hands-on-science and grade-level
          STEM Enrichment in schools
        image: /uploads/news-stem.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Mental Health
        heading: Mental Health
        description: >-
          ASF Funds Mental Health supports K-5, at AMS and AHS and Social
          Emotional Programs at all schools
        image: /uploads/news-mental.jpg
        link_url: /programs/
        link_text: Read more
        meta: >-
          <span>Photo by <a href="https://www.pexels.com/@diohasbi"
          target="new">Dio Hasbi Saniskoro</a> from <a
          href="https://www.pexels.com/photo/people-doing-group-hand-cheer-3280130/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels"
          target="new">Pexels</a></span>
      - group: Early Learning
        heading: Preschool Scholarships
        description: >-
          ASF offers grant funds to help make the cost of preschool affordable
          for all families
        image: /uploads/news-prek.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Scholarships
        heading: Over $356,000 awarded in 2023
        description: >-
          149 scholarships awarded to graduating seniors and current college
          students
        image: /uploads/News-2023-scholarships.jpg
        link_url: /scholarships/
        link_text: Read more
        meta: <span>Photo by EJ Harris</span>
      - group: Events
        heading: Ready to Learn Fair
        description: >-
          August 19, 2023 - Providing back-to-school essentials to over 500
          students who need a little extra support
        image: /uploads/news-rtl2.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: Summer Learning
        heading: Impact
        description: 4 Weeks of Targeted Support for At-Risk Students K-5
        image: /uploads/news-impact.jpg
        link_url: /programs/
        link_text: Read more
        meta:
      - group: COVID Relief
        heading: Fueling Education Fun Run
        description: >-
          Each September, we partner with Puget Sound Refinery to kick off the
          school year, bring the community together and help raise funds
        image: /uploads/news-FR5k.jpg
        link_url: /programs/run
        link_text: Read more
        meta:
  - _bookshop_name: stats
    stats:
      - name: Years of Giving
        text: >-
          <p>Celebrating 41 years of giving! Arnold and Cressa Houle made the
          first gift to the foundation in 1982</p>
        icon: hands-helping
        valueStart: 0
        valueEnd: 41
        refreshInterval: 100
        duration: 2000
        prefix: ''
        suffix: ''
        animation: bounceIn
      - name: Funding Awarded
        text: >-
          <p>Funding awarded for School Year 2022-23 for current programs
          including Impact, Mental Health supports, Pre-School Scholarships,
          STEM enrichment, CTE and vocational programs the Arts and more</p>
        icon: microscope
        valueStart: 1000
        valueEnd: 453460
        refreshInterval: 100
        duration: 2000
        prefix: $
        suffix: ''
        animation: bounceIn
      - name: Ready-to-Learn Fair Backpacks
        text: <p>Supply-filled backpacks delivered at ASF's Ready to Learn Fair</p>
        icon: pencil
        valueStart: 100
        valueEnd: 487
        refreshInterval: 100
        duration: 2500
        prefix: ''
        suffix: ''
        animation: bounceIn
      - name: Total Awarded
        text: Awarded in scholarships <br />Since 1984
        icon: user-graduate
        animation: bounceIn
        refreshInterval: 100
        duration: 2000
        prefix: $
        suffix: ''
        valueStart: 3000
        valueEnd: 3012500
  - _bookshop_name: text-blocks
    blocks:
      - heading: <span>What</span> we do.
        bottom_border: true
        text: >-
          Since it's founding in 1984, ASF has provided a way for parents,
          community members and local businesses to invest in scholarships and
          quality education for students in Anacortes. We strive to provide
          exceptional opportunities district-wide that ignite learning, improve
          educational outcomes and prepare all students to become confident,
          competent and engaged citizens. ASF funds programs in all schools in
          the Anacortes School District, enriching the lives of over 2500
          students each year.
      - heading: <span>How</span> we do it.
        bottom_border: true
        text: >-
          The Anacortes Schools Foundation (ASF), works with the Anacortes
          School District, to help all students achieve the promise of their
          potential. ASF raises money through a variety of events and campaigns
          then uses those funds to support current programs including Early
          Learning, district-wide STEM Enrichment, Social Emotional and Mental
          Health, Summer Learning, Teacher Grants and the Visual and Performing
          Arts. ASF also has a long history of scholarship support and each
          year, through endowed and annual funding, gives over 100 awards and
          over $300,000 in scholarships to graduating Anacortes seniors and
          current college students.
      - heading: <span>Why</span> we do it.
        bottom_border: true
        text: >-
          We believe that education establishes a foundation for opportunity,
          growth and success in life. We raise private support so that our
          public schools have the ability to provide the highest quality
          education for ALL our students. We know that strong, healthy schools
          contribute to a strong and healthy community and we believe that every
          child in Anacortes has a right to the best education available
          anywhere.
  - _bookshop_name: columns
    num_cols: 3
    columns:
      - name: ASF Video
        col_span: 2
        backgroundImage:
        column_components:
          - _bookshop_name: video
            heading: Anacortes Schools Foundation
            youtubeID: kdGyO9yxNXM
            text: >-
              From <a name="Early-Learning" href="/programs">Preschool Early
              Learning</a> to <a name="Scholarships"
              href="/scholarships">College Scholarships</a>, we are committed to
              supporting the Anacortes School District.
      - name: Sidebar
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: important-dates
  - _bookshop_name: news
  - _bookshop_name: marquee
    message:
      heading: Thank You ASF Annual Sponsors!
      text: >-
        We are so grateful to our many community organizations that sponsor ASF
        events.
      url: /giving/sponsor/
    images:
      - name: Alaska Airlines
        url: https://www.alaskaair.com/
        image: /uploads/2023-cts-alaska.jpg
      - name: Anacortes Kayak Tours
        url: https://www.anacorteskayaktours.com/
        image: /uploads/Sponsors_akt.jpg
      - name: Anthony's
        url: https://www.anthonys.com/restaurant/anthonys-at-cap-sante-marina/
        image: /uploads/2021-cts-anthonys.png
      - name: Anacortes Rotary
        url: https://anacortesrotary.org/
        image: /uploads/rotary.png
      - name: Atterberry Gardens
        url: https://www.instagram.com/atterberrygardens/?hl=en
        image: /uploads/2023-cts-atterberry.jpg
      - name: Brandywine Nursery
        url: https://brandywinenursery.com/
        image: /uploads/2023-cts-brandywine.jpg
      - name: Burton Jewelers
        url: https://www.burtonjewelers.com/
        image: /uploads/2022-cts-burton-r.jpg
      - name: Cap Sante Inn
        url: https://www.capsanteinn.net/
        image: /uploads/cap-sante.png
      - name: Dakota Creek
        url: https://dakotacreek.com/
        image: /uploads/dakota-creek-logo.jpg
      - name: Derek Damon Orthodontics
        url: http://www.derekdamonortho.com/
        image: /uploads/2021-cts-damon.png
      - name: Domino's Pizza
        url: https://pizza.dominos.com/washington/anacortes/
        image: /uploads/2023-cts-dominos.jpg
      - name: Edward Jones
        url: https://www.edwardjones.com/us-en/financial-advisor/troy-kunz
        image: /uploads/2023-cts-edj2.jpg
      - name: Fidalgo Creative
        url: https://fidalgocreative.com/
        image: /uploads/Sponsors_fidalgo-creative.png
      - name: Fidalgo Island Rotary
        url: https://fidalgorotary.org/
        image: /uploads/fir-stacked.png
      - name: Freedom Boat Club
        url: >-
          http://info.freedomboatclub.com/dp/sanjuanislands?utm_source=Google+My+Business&utm_medium=GMB+Extension&utm_campaign=GMB+Landing+Page&urlfeed1=What_Are_YOU_Waiting_For
        image: /uploads/fbc-logo.png
      - name: Guemes Island Resort
        url: https://www.guemesislandresort.com/
        image: /uploads/guemes-island-resort-blk-transparent.png
      - name: Haven Financial Partners
        url: >-
          https://www.raymondjames.com/havenfinancialpartners/about-us/anacortes-team
        image: /uploads/2021-cts-haven.png
      - name: Heritage Bank
        url: https://www.heritagebanknw.com/home/home
        image: /uploads/heritage-bank-logo.jpg
      - name: HF Sinclair
        url: https://www.hfsinclair.com/operations/facilities/Puget-Sound-Refinery/
        image: /uploads/2022-cts-sinclair.png
      - name: Island Adventures
        url: https://www.island-adventures.com/
        image: /uploads/sponsor-cts-island.jpg
      - name: Johnny Picasso's
        url: http://johnnypicasso.com/
        image: /uploads/2023-cts-picassos.jpg
      - name: Kiwanis Noon Kiwanis Club
        url: http://www.anacorteskiwanis.org/
        image: /uploads/Sponsors_kiwanis-noon.jpg
      - name: Kiwanis Sunrisers
        url: https://www.facebook.com/AnacortesKiwanis/
        image: /uploads/kiwanis-sunrisers.jpg
      - name: Majestic Inn and Spa
        url: https://www.majesticinnandspa.com/
        image: /uploads/2023-cts-majestic.jpg
      - name: Marathon
        url: >-
          https://www.marathonpetroleum.com/Operations/Refining/Anacortes-Refinery/
        image: /uploads/Sponsors_Marathon.jpg
      - name: MultiGen Wealth Services
        url: https://multigenws.com/
        image: /uploads/2021-cts-multigen2.png
      - name: Matrix Svc Inc
        url: https://www.matrixservice.com/
        image: /uploads/matrix-services-inc.png
      - name: North Sound Oral and Facial Surgery
        url: https://www.northsoundoms.com/
        image: /uploads/northsoundoralsurgery.png
      - name: Paper Pie - Ashler Depner
        url: >-
          https://v3897.paperpie.com/wishlist/9c4ba6c6-ef5c-4786-8811-b05500dfc5f8?fbclid=IwAR1GD7cskKiuVbUgDuR2UXCXoAUH2BA9r2CvcynelvoqFSL_DydIybeK0rI
        image: /uploads/2023-cts-paperpie.jpg
      - name: Playhouse Dental
        url: https://www.playhousedentalkids.com/
        image: /uploads/2021-cts-playhouse.png
      - name: Proscapes
        url: https://www.usproscapes.com/
        image: /uploads/2021-cts-proscapes.png
      - name: Quantum Construction
        url: https://quantumci.com//
        image: /uploads/quantum.png
      - name: Sierra Pacific Industries
        url: https://www.spi-ind.com/
        image: /uploads/sierra-pacific-industries.png
      - name: Swinomish Casino
        url: https://www.swinomishcasinoandlodge.com/
        image: /uploads/edit2-swinomishcasinolodge-logo.png
      - name: Strandberg
        url: https://strandbergconstruction.com/
        image: /uploads/2023-cts-strandberg.jpg
      - name: Synergy Sports (Sportradar)
        url: https://sportradar.com/
        image: /uploads/2023-cts-sportradar.jpg
      - name: T Bailey Inc
        url: https://tbailey.com/
        image: /uploads/sponsor-cts-bailey-r.jpg
      - name: The Port of Anacortes
        url: https://www.portofanacortes.com/
        image: /uploads/poa-logo-clean.png
      - name: Transpac Marinas
        url: https://www.transpacmarinas.com/
        image: /uploads/2021-cts-transpac.png
      - name: Trident
        url: https://www.tridentseafoods.com/
        image: /uploads/2021-cts-trident.png
      - name: Western Refinery
        url: https://wrsweb.com/
        image: /uploads/22-wrs-logo-primary-1.png
      - name: Williams and Nulle CPA
        url: https://www.wncpa.com/
        image: /uploads/williams-and-nulle-logo.jpg
      - name: WR Grace
        url: https://grace.com/
        image: /uploads/logo-grace.png
      - name: Windermere
        url: https://anacortesrealestate.com/
        image: /uploads/windermere.png
      - name: PowerTek
        url: https://www.powertek.net/
        image: /uploads/Sponsors_powertek.jpg
      - name: MP Environmental
        url: https://mpenviro.com
        image: /uploads/Sponsors_mpe.jpg
      - name: Envirogreen Technologies
        url: https://envirogreentech.com
        image: /uploads/copy-of-envirogreen.png
      - name: Veca
        url: https://www.veca.com
        image: /uploads/22-veca-electric.png
      - name: Central Welding Supply
        url: https://www.centralwelding.com/
        image: /uploads/1-5k-cws.jpg
      - name: Christensen Fuels, Lubricants, Propane
        url: https://christensenusa.com/
        image: /uploads/asf-logo-christensen.jpg
      - name: Stronghold Companies
        url: https://www.thestrongholdcompanies.com/
        image: /uploads/1-5k-stronghold-companies.jpg
      - name: OnPoint Industrial Services
        url: https://www.onpoint-us.com/
        image: /uploads/1k-onpoint-color.jpg
    speed: 20000
---
