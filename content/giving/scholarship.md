---
title: "Scholarship Support"
bannerImage: "/uploads/page-title-scholarship.jpg"
description: Contribute to our general scholarship fund or establish a scholarship of your own or in memory of a loved one.
keywords: fund a scholarship, donation, anacortes schools foundation donation, other ways to donate, asf donation, support a scholarship, anacortes scholarships, how can I support a scholarship
featuredImage: /uploads/schol-1.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Scholarship Support
    bannerImage: /uploads/page-title-scholarship.jpg
  - _bookshop_name: stats
    stats:
      - name: Years of Giving
        text: >-
          ASF received its first scholarship donation in 1982, 2 years before officially forming a 501(c)3 organization.
        icon: user-graduate
        valueStart: 2023
        valueEnd: 1982
        refreshInterval: 500
        duration: 2000
        prefix: ''
        suffix: ''
        nosep: true
        countdown: true
        animation: bounceIn
      - name: Funding Awarded
        text: >-
          Awarded in scholarships to students since 1982
        icon: user-graduate
        valueStart: 1000
        valueEnd: 3012500
        refreshInterval: 100
        duration: 2700
        prefix: $
        suffix: ''
        animation: bounceIn
      - name: Individual scholarships awarded
        text: Individual scholarships awarded to students pursuing continued education
        icon: user-graduate
        valueStart: 100
        valueEnd: 1442
        refreshInterval: 100
        duration: 2500
        prefix: ''
        suffix: ''
        nosep: true
        animation: bounceIn
  - _bookshop_name: content
    title: ASF Scholarship Program
    sectionclass: "py-8"
    content: |
      ASF has a rich history of providing scholarships to Anacortes students and a robust current program. In June 2022, ASF awarded 132 scholarships to 105 students.

      ## Featured Scholarships from Barrett Financial

      {{< asf-figure title="Barrett Financial Logo" divclass="w-full md:w-auto md:float-right md:ml-8" figclass="mx-auto md:mx-0" imgclass="" capclass="" src="/uploads/donor-Barrett-crop.jpg" width=200 link="https://www.barrettfinancialltd.com" >}}
      
      [Barrett Financial](https://www.barrettfinancialltd.com/) is a wealth management firm celebrating 30+ years of business in Anacortes, WA. Barrett is committed to giving back to the Anacortes community and believes that we can work together to inspire a path to a better future. Barrett also believes that access to higher education is an important measure of progress and proudly hopes to provide a leg-up towards increased educational mobility.

      ### Barrett Financial First in Family Scholarship
      
      The [Barrett Financial First in Family Scholarship](/scholarships/barrett) is a $5,000 annual scholarship offered to an Anacortes High School graduating senior who is a first generation college student who plans to attend a 4-year college. This award focuses on high-achievers that combine stellar academic performance with extracurricular success.
      
      ### Barrett Financial Student of Color Scholarship

      The [Barrett Financial Student of Color Scholarship](/scholarships/barrett-poc/) is a $5,000 annual scholarship offered to an Anacortes High School graduating senior who identifies as a Person of Color and who plans to attend a 4-year college. This award focuses on high-achievers that combine stellar academic performance with extracurricular success.
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Steps
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: steps
            title: Scholarships are funded each year in two ways
            text: 
            title_url: 
            steps:
              - heading: Annual Scholarship
                subheading: $1000 or more
                url: 
                text: Donation/s from individual or corporate donors are received by ASF each year, then distributed as scholarships. Some scholarships are awarded as "one time only," while others are awarded every year.
                action: 
                action_url: 
              - heading: Endowed Scholarship
                subheading: Minimum of $20,000 (payable to ASF over up to 4 years)
                url: 
                text: Annual scholarship funds are provided through earnings on endowments which are held by ASF and managed by a professional investment management company. The corpus remains in full and, pursuant to the donors wishes, the scholarship may continue in perpetuity. Funds available for scholarships may vary from year to year, depending on investment performance.
                action: 
                action_url: 
      - name: Slides
        col_span: 1
        backgroundImage: 
        column_components:
          - _bookshop_name: content
            content: |
              {{< asf-figure title="Chamber of Commerce Business Scholarship - Jesica Kiser presents to James McClellan" divclass="w-full" figclass="mx-auto" caption="Chamber of Commerce Business Scholarship - Jesica Kiser presents to James McClellan." capclass="" src="/uploads/20230605_ASF_scholarship_awards-62.jpg" width=650 >}}
  - _bookshop_name: media-text
    image_right: false
    heading: How are ASF Scholarships awarded?
    text: |
      Each spring, students are invited to apply online for ASF scholarships. Students submit one common application and are considered for over 120 individual scholarships. Depending on the type of scholarship students may be asked to include grades, test scores (optional), area of career interest, extracurricular activities, community involvement, financial need, etc.

      The ASF Scholarship Selection Committee reviews the applications and the available scholarships and decides on the awards. The donors, if they desire, are notified of the award recipient. However, IRS rules do not allow the donor to select the scholarship recipients.

      Some scholarships are awarded based on academic performance, while others are based on areas of interest or financial need. Some are awarded for students pursuing an education in a specific field of study (such as STEM, nursing, the arts, international studies, etc.). Some are awarded based on a student's demonstrated willingness and ability to help others. Most are awarded on a combination of criteria (with some criteria to serve as “tie-breakers.”)  
    url: 
    action: 
    captions: true
    images:
      - title: AAUW and other community organizations partner with ASF to award scholarships.
        src: /uploads/20230605_ASF_scholarship_awards-19.jpg
        caption: Three STEM Awards were presented to Ellie Feist, Severa Kullenkamp and Abigail Ball.
  - _bookshop_name: cognito
    key: UyfjasXdoU6KNNOYinFa-w
    number: 8
---
