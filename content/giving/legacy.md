---
title: Make a Legacy Gift
aliases:
  - /giving/planned-giving.html
  - /giving/endowments.html
bannerImage: /uploads/page-title-legacy.jpg
description: >-
  Leave a legacy of education with planned giving through a will or retirement
  plan/IRA with Anacortes Schools Foundation's planned gift opportunities.
keywords: >-
  donation, anacortes schools foundation donation, leave a donation in a will,
  other ways to donate, asf donation, make a donation online
featuredImage: /uploads/legacy-1.jpg
layout: blocks
content_blocks:
  - _bookshop_name: hero
    title: Make a Legacy Gift
    bannerImage: /uploads/page-title-legacy.jpg
  - _bookshop_name: heading
    title: About Anacortes Schools Foundation
    subtitle:
    text: >-
      Leave a legacy of education with planned giving through a will or
      retirement plan/IRA with Anacortes Schools Foundation's planned gift
      opportunities.
    sectionclass: text-center
  - _bookshop_name: steps
    horizontal: true
    steps:
      - heading: Wills/Trusts
        text: >-
          A bequest is a written statement in a donor's will or trust directing
          that speciﬁc assets, or a percentage of the estate, will be
          transferred to charity at the donor's death. They can take many forms,
          donating stocks, retirement accounts, IRAs, cash, insurance, etc and
          are completely controlled by the donor until their death. Donors can
          leave outright gifts to the Anacortes Schools Foundation in their will
          or trust agreement or establish gift annuities and trusts by means of
          such instruments.
      - heading: Retirement Plans and IRAs
        text: >-
          A second type of expectancy involves naming an organization as the
          beneﬁciary of a retirement plan or IRA. For many individuals,
          retirement plan assets represent the single largest asset in their
          portfolios. This gifting opportunity merely involves obtaining a
          beneﬁciary designation form from the retirement plan administrator and
          naming a charity as the entire, or partial, beneﬁciary of the
          retirement plan assets upon the owner's death. A donor may achieve
          signiﬁcant income and estate tax savings by naming a charity as the
          beneﬁciary of the retirement plan assets.
  - _bookshop_name: columns
    num_cols: 2
    columns:
      - name: Background
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title: ASF Legacy Society
            content: >
              The ASF Legacy Society will identify and recognize individuals who
              have included ASF in their will or estate plan (unless the donor
              desires anonymity). All planned gifts qualify one for membership,
              regardless of dollar amount and regardless of whether the
              commitment is revocable or irrevocable. ASF requests that donors
              provide a copy of the legal document that references the donor's
              gift, but documents are not required for listing in the Legacy
              Society. In the absence of the above, ASF requires a written
              statement from the donor summarizing the gift arrangement.


              ASF will always remain cognizant of the donor's needs and desires
              and reserves the right to refuse any planned gift, or enter into
              any planned gift arrangement, that is not in ASF's best interest.
          - _bookshop_name: content
            title: Lasting Impact
            content: >-
              Your investment in students, public education, teachers and
              ultimately the community, is one of the most impactful investments
              you can make.


              To find out more about why your gift is important, contact
              [president@asfkids.org](mailto:president@asfkids.org).
      - name: Highlight
        col_span: 1
        backgroundImage:
        column_components:
          - _bookshop_name: content
            title: First to Join
            content: >
              {{< asf-figure src="/uploads/DSC_0291.jpg" imgclass="w-full"
              caption="Greg and Sue Monaghan Cap Sante Scholarship awarded to
              Kylee Minter in 2018" capclass="text-center" width=600 >}}


              The first donors to join the ASF Legacy Society were Anacortes
              residents **Greg and Sue Monaghan**. The Monaghans believe “few
              causes in life can have a greater influence on a young person's
              journey than the gift of a formal education or advanced training
              in a trade.” They have pledged to endow a scholarship for a
              student graduating from Cap Sante High School through their
              estate. And, because they want to see their giving in action and
              meet some of their recipients, they are also funding an annual
              scholarship. 


              Endowments like the Monaghan's are a means for donors to
              effectively support educational excellence in the Anacortes School
              District and leave a lasting legacy in the community. ASF is
              governed by a board of directors who provide stewardship for, and
              disburse the earnings from, these endowment funds. Donors may
              designate earnings for a specific purpose like scholarships, STEM,
              or early learning, or allow ASF to determine where the needs are
              greatest.
  - _bookshop_name: cognito
    key: UyfjasXdoU6KNNOYinFa-w
    number: 9
  - _bookshop_name: content
    title:
    content: >
      Please note that no information on this website should be considered as
      the rendering of legal, accounting or other professional advice. Please
      consult your personal tax and financial advisors before implementing a
      planned gift to charity, including one to the Anacortes Schools
      Foundation. The Anacortes Schools Foundation is a local 501(c)3 local
      non-profit, charity that serves students, and teachers in the Anacortes
      School District. Our Tax ID Number is 91-1263495. ASF shall not give legal
      or advice or specific tax information to prospective donors. All
      information about a donor or named income beneﬁciaries, including names,
      ages, gift amounts, and net worth should be kept strictly conﬁdential by
      the organization unless permission is obtained from the donor to release
      such information.
    sectionclass: text-sm
---
