baseURL = "/"
contentdir    = "content"
layoutdir     = "layouts"
publishdir    = "public"
title = "Anacortes Schools Foundation"
canonifyurls  = true
paginate = 6

DefaultContentLanguage = "en"
timeZone = "America/Los_Angeles"
metaDataFormat = "yaml"
pygmentsUseClasses = true
pygmentCodeFences = true
#disqusShortname = "XXX"
#googleAnalytics = "XXX"

[outputs]
   home = [ "HTML", "JSON" ]

[build]
  writeStats = true

preservetaxonomynames = true

[taxonomies]
  category = "categories"
  area = "areas"

[markup]
  defaultMarkdownHandler = "goldmark"
  [markup.goldmark]
    [markup.goldmark.extensions]
      definitionList = true
      footnote = true
      linkify = true
      strikethrough = true
      table = true
      taskList = true
      typographer = false
    [markup.goldmark.parser]
      autoHeadingID = true
      autoHeadingIDType = "github"
      [markup.goldmark.parser.attribute]
        block = true
        title = true
    [markup.goldmark.renderer]
      hardWraps = false
      unsafe = true
      xhtml = false
  [markup.highlight]
    anchorLineNos = false
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineAnchors = ""
    lineNoStart = 1
    lineNos = false
    lineNumbersInTable = true
    noClasses = true
    style = "monokai"
    tabWidth = 4
  [markup.tableOfContents]
    endLevel = 3
    ordered = false
    startLevel = 2

[Params]
  subtitle = "Anacortes Schools Foundation"
  logo = "img/avatar-icon.png"
  favicon = "img/favicon.ico"
  dateFormat = "January 2, 2006"
  commit = false
  rss = true
  comments = true
  soonURL = "/search"
  soonText = "(Search bar coming soon)"
  applicationsOpen = false
  applicationFormLink = "https://www.cognitoforms.com/AnacortesSchoolsFoundation/_2023GeneralScholarshipApplication"

#  gcse = "012345678901234567890:abcdefghijk" # Get your code from google.com/cse. Make sure to go to "Look and Feel" and change Layout to "Full Width" and Theme to "Classic"

#[[Params.bigimg]]
#  src = "img/triangle.jpg"
#  desc = "Triangle"
#[[Params.bigimg]]
#  src = "img/sphere.jpg"
#  desc = "Sphere"
#[[Params.bigimg]]
#  src = "img/hexagon.jpg"
#  desc = "Hexagon"

## Menu

[[menu.main]]
    name = "HOME"
    url = "/"
    weight = 1

[[menu.main]]
    identifier = "what-we-do"
    name = "What We Do"
    weight = 2

[[menu.main]]
    parent = "what-we-do"
    name = "All Programs"
    url = "/programs/"
    weight = 1

[[menu.main]]
    parent = "what-we-do"
    identifier = "prog-celebrate"
    name = "Celebrate the Season"
    url = "/programs/celebrate-the-season/"
    weight = 2  

[[menu.main]]
    parent = "what-we-do"
    identifier = "prog-holiday-fair"
    name = "Holiday Family Fair"
    url = "/programs/holiday-fair/"
    weight = 3
    
[[menu.main]]
    parent = "what-we-do"
    identifier = "prog-fun-run"
    name = "Fueling Education Fun Run"
    url = "/programs/run/"
    weight = 6

[[menu.main]]
    parent = "what-we-do"
    identifier = "prog-rtl"
    name = "Ready to Learn Fair"
    url = "/programs/readytolearn/"
    weight = 7

[[menu.main]]
    identifier = "news"
    name = "News"
    weight = 3

[[menu.main]]
    parent = "news"
    name = "All News"
    url = "/news/"
    weight = 1

[[menu.main]]
    parent = "news"
    name = "COVID"
    url = "/categories/covid/"
    weight = 2

[[menu.main]]
    parent = "news"
    name = "Scholarship"
    url = "/categories/scholarships/"
    weight = 3

[[menu.main]]
    parent = "news"
    name = "STEM"
    url = "/categories/stem/"
    weight = 4

[[menu.main]]
    identifier = "scholarships"
    name = "Scholarships"
    weight = 3

[[menu.main]]
    parent = "scholarships"
    name = "Browse All Available"
    url = "/scholarships/"
    weight = 1

[[menu.main]]
    parent = "scholarships"
    name = "How To Apply"
    url = "/scholarships/apply/"
    weight = 2

[[menu.main]]
    parent = "scholarships"
    name = "Questions?"
    url = "/scholarships/faq/"
    weight = 3

[[menu.main]]
    parent = "scholarships"
    name = "Student Information Form"
    url = "/scholarships/student-info/"
    weight = 4    

[[menu.main]]
    identifier = "giving"
    name = "Giving"
    url = "#"
    weight = 5

[[menu.main]]
    parent = "giving"
    name = "Make a Donation"
    url = "/giving/"
    weight = 1

[[menu.main]]
    parent = "giving"
    name = "Legacy Donation"
    url = "/giving/legacy/"
    weight = 2

[[menu.main]]
    parent = "giving"
    name = "Be a Sponsor"
    url = "/giving/sponsor/"
    weight = 3

[[menu.main]]
    parent = "giving"
    name = "Scholarship Support"
    url = "/giving/scholarship/"
    weight = 4

[[menu.main]]
    identifier = "about-asf"
    name = "ABOUT ASF"
    url = "#"
    weight = 6

[[menu.main]]
    parent = "about-asf"
    name = "About Us"
    url = "/about/"
    weight = 1

[[menu.main]]
    parent = "about-asf"
    name = "Volunteer with Us"
    url = "/volunteer/"
    weight = 1

[[menu.main]]
    parent = "about-asf"
    name = "Contact Us"
    url = "/contact/"
    weight = 1

# Modules for Hugo Bookshop

[module]
replacements = "components.local -> ../component-library"

[[module.imports]]
path = 'components.local'

[[module.imports]]
path = 'github.com/cloudcannon/bookshop/hugo/v3'