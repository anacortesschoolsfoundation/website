const fs = require("fs");

module.exports = {
  engines: {
    "@bookshop/hugo-engine": {
      extraFiles: {
        "layouts/shortcodes/asf-figure.html": fs.readFileSync("layouts/shortcodes/asf-figure.html", { encoding: "utf8" }),
        "layouts/shortcodes/apply-button.html": fs.readFileSync("layouts/shortcodes/apply-button.html", { encoding: "utf8" }),
        "layouts/shortcodes/button.html": fs.readFileSync("layouts/shortcodes/button.html", { encoding: "utf8" }),
        "layouts/partials/news_latest.html": fs.readFileSync("layouts/partials/news_latest.html", { encoding: "utf8" }),
        "layouts/partials/prevnext.html": fs.readFileSync("layouts/partials/prevnext.html", { encoding: "utf8" }),
        "layouts/partials/scholarships_paginated.html": fs.readFileSync("layouts/partials/scholarships_paginated.html", { encoding: "utf8" }),
        "layouts/partials/pagination.html": fs.readFileSync("layouts/partials/pagination.html", { encoding: "utf8" }),
        "layouts/partials/section_list.html": fs.readFileSync("layouts/partials/section_list.html", { encoding: "utf8" }),
        "config/production/hugo.toml": fs.readFileSync("config/production/hugo.toml", { encoding: "utf8" })
      }
    }
  }
}
